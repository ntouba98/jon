import JON, {
    Validator,
    Number,
} from '../jon';
import Timez from '@hivi/timez/timez';

const val = undefined;
const schema = new JON.ChosenType().choices(
    new JON.Enum().choices('20', 15, 'tRue'),
    new JON.Boolean(),
    new JON.String().identifier(),
);

const res = JON.Validator.validate(
    schema,
    val
);

console.log(`> hivi.jon.feature | res.data:: `, res.data);
console.log(`> hivi.jon.feature | res.valid:: `, res.valid);
console.log(`> hivi.jon.feature | res.error:: `, res.error);
// console.log(`> hivi.jon.feature | res.error:: `, res.error);