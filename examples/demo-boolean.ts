import JON, {
    Validator,
    Number,
} from '../jon';

const val = 0;
const schema = new JON.Boolean().setLabel('chaine1'); // .trueValues(['yes']).falseValues(['no']);

const res = JON.Validator.validate(
    schema,
    val
);

console.log(`> hivi.jon.feature | val:: `, val);

console.log(`> hivi.jon.feature | res.data:: `, res.data);
console.log(`> hivi.jon.feature | res.valid:: `, res.valid);
console.log(`> hivi.jon.feature | res.error:: `, res.error);
// console.log(`> hivi.jon.feature | res.error:: `, res.error);