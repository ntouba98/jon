import JON, {
    Validator,
    Number,
} from '../jon';
import Timez from '@hivi/timez/timez';

const val = '2021-06-20 15:07:44.011 GMT+2';
const schema = new JON.Date()
.setLabel('date1')
.format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]')
.min('2021-05-20 15:07:44.010 GMT+2')
 .max('2021-07-20 15:07:44.015 GMT+2');
// .less('2021-05-24 15:07:44.010 GMT+2')
// .greater('2021-07-20 15:07:44.012 GMT+2');

const res = JON.Validator.validate(
    schema,
    val
);

console.log(`> hivi.jon.feature | res.data:: `, res.data);
console.log(`> hivi.jon.feature | res.valid:: `, res.valid);
console.log(`> hivi.jon.feature | res.error:: `, res.error);
// console.log(`> hivi.jon.feature | res.error:: `, res.error);