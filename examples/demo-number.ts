import JON, {
    Validator,
    Number,
} from '../build/jon';

const val = '50';
// const schema = new JON.Number().min(3).max(8);
// const schema = new JON.Number().less(3).greater(8);
// const decimalPart = (parseFloat(String(val))) ? parseFloat(String(val)) - Math.trunc(parseFloat(String(val))) : undefined;
// const schema = new JON.Number().positive();
// const schema = new JON.Number().decimal(3);
// const schema = new JON.Number().setLabel('nombre1').integer();
// const schema = new JON.Number().setLabel('nombre1').multiple(3);
const schema = new JON.Number().setLabel('nombre1').setError('custom error').applyMapping((value: number | null) => !!value ? value + 5 : null)// .TCPPort();

const res = JON.Validator.validate(
    schema,
    val
);

console.log(`> hivi.jon.feature | res.data:: `, res.data);
console.log(`> hivi.jon.feature | res.valid:: `, res.valid);
console.log(`> hivi.jon.feature | res.error:: `, res.error);