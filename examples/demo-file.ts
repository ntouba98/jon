import JON, {
    Validator,
    Number,
} from '../build/jon';

import fs from 'fs';
import { Blob } from "buffer";


const val: Blob | null | undefined = null;
const schema = new JON.File().isUndefined();

const res = JON.Validator.validate(
    schema,
    val
);

console.log(`> hivi.jon.feature | val:: `, val);
console.log(`> hivi.jon.feature | res.data:: `, res.data);
console.log(`> hivi.jon.feature | res.valid:: `, res.valid);
console.log(`> hivi.jon.feature | res.error:: `, res.error);
// console.log(`> hivi.jon.feature | res.error:: `, res.error);