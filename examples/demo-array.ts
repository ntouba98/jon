import JON, {
    Validator,
    Number,
} from '../jon';
import Timez from '@hivi/timez/timez';

const val = null as any;
const schema = new JON.Array().types(
    new JON.File(),
).isUndefined();

const res = JON.Validator.validate(
    schema,
    val
);

console.log(`> hivi.jon - array | res.data:: `, res.data);
console.log(`> hivi.jon - array | res.valid:: `, res.valid);
console.log(`> hivi.jon - array | res.error:: `, res.error);
// console.log(`> hivi.jon - array | res.error:: `, res.error);