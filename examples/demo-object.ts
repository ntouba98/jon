import fs from 'fs';
import { Blob } from "buffer";
import JON, {
    Validator,
    Number,
} from '../jon';
import Timez from '@hivi/timez/timez';

/*
const val = {
    name: 'bilong ntouba',
    exists: '1',
    prename: 'celestin',
    password: 'filbill1998',
    confirm_password: 'filbill1998',
};
const schema = new JON.Object().struct({
    name: new JON.String().min(2).required(),
    exists: new JON.Boolean(),
    prename: new JON.Clone('name'),
    password: new JON.String().min(5).required(),
    confirm_password: new JON.Ref('password'),
})// .nolink('password', 'confirm_password');
*/
/*
const val = {
    data: {
        createdat: '2022-03-18T22:45:32.595Z',
        updatedat: '2022-03-18T22:45:32.595Z',
        publishedat: '2022-03-18T22:45:32.595Z',
        id: 'epbffd8e7z',
        title: 'element 1',
        description: "description de l'element 1",
        size: 7,
        stable: true,
        published: true,
        blocked: false,
        parentid: null,
        blockedat: null
    },
    msg: { type: 'success' }
};
const schema = new JON.Object().struct({
    data: new JON.Object(),
    msg: new JON.Object().struct({
        type: new JON.Enum().choices('success')
    })
});
*/
/*
const val = {
    name: 'bilong ntouba',
    exists: '1',
    prename: 'celestin',
    surname: 'billy',
    password: 'filbill1998',
    confirm_password: 'filbill1998',
};
const schema = new JON.Object().struct({
    name: new JON.String().min(2).isUndefined().required(),
    exists: new JON.Boolean().isUndefined(),
    prename: new JON.Clone('name'),
    surname: new JON.String().min(2).isUndefined().isUndefined(),
    password: new JON.String().min(5).isUndefined().required(),
    confirm_password: new JON.Ref('password'),
})// .nolink('password', 'confirm_password');
// const schema = new JON.Object().requiredAttributes('name', 'exists', 'password', 'confirm_password');
*/
// const val = {title: 'Authentification Requise', message: 'Vous devez vous connecté pour continuer'};
// const schema = new JON.Object().struct({
//     title: new JON.String().required(),
//     message1: new JON.String().required(),
// }).required();
// 
// const res = JON.Validator.validate(
//     schema,
//     val
// );


const permissions = [
    "add_arti",
    "add_arca",
    "add_user",
    "arcorrest_arti",
    "arcorrest_arca",
    "arcorrest_arty",
    "aorr_authz",
    "aorr_clt",
    "aorr_pare",
    "aorr_token",
    "aorr_user",
    "bloorunb_arti",
    "bloorunb_arca",
    "bloorunb_arty",
    "bloorunb_authz",
    "bloorunb_clt",
    "bloorunb_pare",
    "bloorunb_token",
    "bloorunb_user",
    "delete_arti",
    "delete_arca",
    "delete_arty",
    "delete_authz",
    "delete_clt",
    "delete_pare",
    "delete_token",
    "delete_user",
    "edit_arti",
    "edit_arca",
    "edit_authz",
    "edit_clt",
    "editmulti_user",
    "edit_user",
    "exists_arti",
    "exists_arca",
    "exists_arty",
    "exists_authz",
    "exists_clt",
    "exists_perm",
    "exists_pare",
    "exists_role",
    "exists_token",
    "exists_user",
    "exists_usty",
    "existsv_arti",
    "existsv_arca",
    "existsv_arty",
    "existsv_authz",
    "existsv_clt",
    "existsv_perm",
    "existsv_pare",
    "existsv_role",
    "existsv_token",
    "existsv_user",
    "existsv_usty",
    "findall_arca",
    "findall_arty",
    "findall_arti",
    "findall_authz",
    "findall_clt",
    "findall_perm",
    "findall_pare",
    "findall_role",
    "findall_token",
    "findall_usty",
    "findall_user",
    "findallv_arca",
    "findallv_arty",
    "findallv_arti",
    "findallv_authz",
    "findallv_clt",
    "findallv_perm",
    "findallv_pare",
    "findallv_role",
    "findallv_token",
    "findallv_usty",
    "findallv_user",
    "find_arti",
    "find_arca",
    "find_arty",
    "find_authz",
    "find_clt",
    "find_perm",
    "find_pare",
    "find_role",
    "find_token",
    "find_user",
    "find_usty",
    "findv_arti",
    "findv_arca",
    "findv_arty",
    "findv_authz",
    "findv_clt",
    "findv_perm",
    "findv_pare",
    "findv_role",
    "findv_token",
    "findv_user",
    "findv_usty",
    "gen_token",
    "genekey_pare",
    "init_arca",
    "init_arca",
    "init_arty",
    "init_perm",
    "init_role",
    "init_user",
    "init_usty",
    "refresh_token",
    "remusr_token",
    "remusrs_token",
    "update_arti",
    "update_arca",
    "upwireco_pare",
    "update_user",
    "validreq_pare"
];
const roles = [
    "manage_arti",
    "manage_arca",
    "manage_arty",
    "manage_authz",
    "manage_clt",
    "manage_perm",
    "manage_pare",
    "manage_role",
    "manage_token",
    "manage_user",
    "manage_usty",
    "role001",
    "role002"
];
const status = [
    "visible",
    "archived"
];
const users = [
    "retailer01",
    "user1"
];
const lang: 'fr' = 'fr';

/* const schema = new JON.Object(lang).struct({
    search: new JON.String(lang)
      .max(50)
      .toLowercase()
      .isUndefined(),
    slug: new JON.String(lang)
      .max(50)
      .toLowercase()
      .isUndefined(),
    status: new JON.Enum(lang).choices(...status).isUndefined(),

    // permissions: new JON.ChosenType(lang).choices(
    //   new JON.Array(lang).types(
    //     new JON.Enum(lang).choices(...permissions).required(),
    //   ).required(),
    //   new JON.Enum(lang).choices(...permissions).required(),
    // ).isUndefined(),
// 
    // roles: new JON.ChosenType(lang).choices(
    //   new JON.Array(lang).types(
    //     new JON.Enum(lang).choices(...roles).required(),
    //   ).required(),
    //   new JON.Enum(lang).choices(...roles).required(),
    // ).isUndefined(),

    users: new JON.ChosenType(lang).choices(
      new JON.Array(lang).types(
        new JON.Enum(lang).choices(...users).required(),
      ).required(),
      new JON.Enum(lang).choices(...users).required(),
    ).isUndefined(),
});
const val: any = {
    // "permissions": [ "arcorrest_arty" ],
    // "roles": [ "manage_pare" ],
    "users": [ "user1" ],
}; */

// const schema = new JON.Object(lang).struct({
//     file: new JON.File(),
//     location: new JON.Object(lang).isUndefined().struct({
//         lng: new JON.Number(lang).isUndefined(),
//         lat: new JON.Number(lang).isUndefined(),
//     })
// });
// const val = {
//     file: new Blob(
//         [fs.readFileSync('B:\\conserver\\projects\\mix-app\\hivienv\\hivi_backend\\main\\static\\logo.app.png')]
//     ),
// }

const schema = new JON.Object(lang).struct({
    covers: new JON.Object(lang).struct({
        primary: new JON.File(lang).isUndefined(),
        secondary: new JON.Array(lang).types(
            new JON.File(lang),
        ).isUndefined(),
    }),
    usertype: new JON.Enum(lang).choices(...['a', 'b']).isUndefined(),
});
const val = {
    covers: {
        primary: new Blob(
            [fs.readFileSync('B:\\conserver\\projects\\mix-app\\hivienv\\hivi_backend\\main\\static\\logo.app.png')]
        ),
        secondary: [
            new Blob(
                [fs.readFileSync('B:\\conserver\\projects\\mix-app\\hivienv\\hivi_backend\\main\\static\\logo.app.png')]
            )
        ],
    },
}

const res = JON.Validator.validate(
    schema,
    val
);

/* const schema = new JON.Enum(lang).choices(...users).isUndefined();
const val: any = "user1";

const res = JON.Validator.validate(
    schema,
    val
); */

console.log(`> hivi.jon.feature | res.data:: `, res.data);
console.log(`> hivi.jon.feature | res.valid:: `, res.valid);
console.log(`> hivi.jon.feature | res.error:: `, res.error);
// console.log(`> hivi.jon.feature | res.error:: `, res.error);