
var { series, dest, src } = require('gulp');
var del = require('del');
var ts = require('gulp-typescript');
var uglify = require('gulp-uglify-es').default;
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var htmlmin = require('gulp-htmlmin');
var minify = require('gulp-minify');
var ignoreErrors = require('gulp-ignore-errors');


var tsProject = ts.createProject('tsconfig.json');
var AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];


function TSscripts() {
    return src(['./**/*.ts', '!./build/**/*', '!./node_modules/**/*', '!./examples/**/*'])
    .pipe(tsProject())
    .pipe(dest('./build-ts'));
}
function scripts() {
    return src('./build-ts/**/*.js')
    .pipe(uglify())
    .pipe(minify({
        ext:{
            min:'.js'
        },
    }))
    .pipe(dest('./build'));
}
function copydTSscripts() {
  return src(['./build-ts/**/*.d.ts'])
    .pipe(dest('./build'));
}
function styles() {
    return src(['./**/*.css', '!./build/**/*', '!./node_modules/**/*', '!./examples/**/*'])
    .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
    .pipe(csso())
    .pipe(dest('./build'));
}
function pages() {
    return src(['./**/*.html', '!./build/**/*', '!./node_modules/**/*', '!./examples/**/*'])
    .pipe(htmlmin({
        collapseWhitespace: true,
        removeComments: true
    }))
    .pipe(dest('./build'));
}
function vuePages() {
    return src(['./**/*.vue', '!./build/**/*', '!./node_modules/**/*', '!./examples/**/*'])
    .pipe(dest('./build'));
}
function otherFiles() {
    return src(['./**/*.json', './README.md', '!./build/**/*', '!./node_modules/**/*', '!./examples/**/*'])
    .pipe(dest('./build'));
}

function clean() {
    return del([
        'build',
        'build-ts',
        'build-old'
    ]);
}
function cleanAfter() {
    return del(['build-ts']);
}


exports.tsscripts = TSscripts;
exports.copydtsscripts = copydTSscripts;
exports.scripts = scripts;
exports.styles = styles;
exports.clean = clean;
exports.default = series(
    clean,
    TSscripts,
    copydTSscripts,
    scripts,
    styles,
    pages,
    vuePages,
    otherFiles,
    cleanAfter
);
exports.default2 = series(
    // clean,
    // TSscripts,
    copydTSscripts,
    scripts,
    styles,
    pages,
    vuePages,
    otherFiles,
    cleanAfter
);