# @hivi/jon

## Presentation

**@hivi/jon** is a dependency allowing you to validate multiple data types in a precise and fast way.

## Installation

```console
npm i @hivi/jon -s
```

## Use

### Number

```js
import JON, {
    Validator,
    Number,
} from '@hivi/jon/jon';

const val = 50;
const schema = new JON.Number();
const res = JON.Validator.validate(
    schema,
    val
);

console.log(`> @hivi/jon | res.data:: `, res.data);
console.log(`> @hivi/jon | res.valid:: `, res.valid);
console.log(`> @hivi/jon | res.error:: `, res.error);
```

#### .min(min_value)

```js
const schema = new JON.Number().min(3);
```

##### Arguments

| Property | Description | Type |
| :-------- | :---------- | :--- |
| **min_value** | the number to be validated must be less than or equal to min_value | **Number** |

## Useful links

* [Git](/ "https://ntouba98@bitbucket.org/ntouba98/jon.git")
* [Nodejs dependency](/ "https://www.npmjs.com/package/@hivi/jon")