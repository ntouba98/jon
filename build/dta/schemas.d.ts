/// <reference types="node" />
import { Blob } from 'buffer';
import { JONRule, JONRes } from './interface';
declare class JONDefaultSchema<T = any> {
    protected _label: string | undefined;
    protected _lang: 'fr' | 'en';
    protected _options: {
        validationType: ('number' | 'boolean' | 'string' | 'object' | 'list' | 'date' | 'enum' | 'file' | 'any');
        type: ('number' | 'boolean' | 'string' | 'object' | 'list' | 'date' | 'enum' | 'file' | 'any');
        instance?: (Date | Blob);
        rules: JONRule[];
    };
    protected _finalError: Error;
    protected _customError: Error | undefined;
    protected _params: {};
    protected _defaultValue: T | undefined;
    private _isUndefined;
    private _map;
    constructor(lang?: 'fr' | 'en');
    options(): {
        validationType: "string" | "number" | "boolean" | "object" | "list" | "date" | "enum" | "file" | "any";
        type: "string" | "number" | "boolean" | "object" | "list" | "date" | "enum" | "file" | "any";
        instance?: Date | Blob | undefined;
        rules: JONRule[];
    };
    private validator;
    protected get rules(): any;
    protected set rules(rules: any);
    get lang(): 'fr' | 'en';
    set lang(lang: 'fr' | 'en');
    protected getIsUndefined(): boolean;
    isUndefined(): this;
    getLabel(): string | undefined;
    setLabel(label: string): this;
    protected getError(): Error;
    protected setFinalError(errorValue: any): void;
    setError(errorValue: any): this;
    protected ruleNames: () => string[];
    protected addOtherRules(rules: JONRule[]): void;
    required(): this;
    default(defaultValue: T): this;
    getDefault(): T | undefined;
    error(value: any): Error | undefined;
    isValid(value: any): boolean;
    sanitize(value: any): any | undefined;
    validate(value: any): JONRes;
    enum(...choices: T[]): this;
    enumNot(...choices: T[]): this;
    protected checkIsUndefined(value: any): boolean;
    applyApp(name?: string, init?: (value: any) => any, rule?: ((value: any) => JONRes) | undefined, sanitize?: (value: JONRes) => any): this;
    applyMapping(mapAction: ((value: T) => T)): this;
}
export declare class JONAnySchema extends JONDefaultSchema<any> {
    constructor(lang?: 'fr' | 'en');
    private init;
    private initRule;
}
export declare class JONObjectSchema extends JONDefaultSchema<any> {
    private _struct;
    private _primaryStruct;
    private _links;
    private _nolinks;
    private _lengthValue?;
    private _maxValue?;
    private _minValue?;
    private _lessValue?;
    private _greaterValue?;
    constructor(lang?: 'fr' | 'en');
    private init;
    private initRule;
    length(lengthValue: number): this;
    min(minValue: number): this;
    max(maxValue: number): this;
    less(lessValue: number): this;
    greater(greaterValue: number): this;
    requiredAttributes(...attributes: string[]): this;
    flexibleAttributes(...attributes: string[]): this;
    struct(values: any): this;
    primaryStruct(values: any): this;
    getStruct(): any;
    link(target: string, item: string): this;
    nolink(target: string, item: string): this;
}
export declare class JONArraySchema extends JONDefaultSchema<any[] | null> {
    private _types;
    private _lengthValue?;
    private _maxValue?;
    private _minValue?;
    private _lessValue?;
    private _greaterValue?;
    constructor(lang?: 'fr' | 'en');
    private init;
    private initRule;
    length(lengthValue: number): this;
    min(minValue: number): this;
    max(maxValue: number): this;
    less(lessValue: number): this;
    greater(greaterValue: number): this;
    types(...values: any[]): this;
    getTypes(): (JONAnySchema | JONObjectSchema | JONNumberSchema | JONStringSchema | JONBooleanSchema | JONDateSchema | JONEnumSchema | JONNotInEnumSchema | JONArraySchema | JONChosenTypeSchema | JONFileSchema)[];
    default(defaultValue: any[]): this;
}
export declare class JONNumberSchema extends JONDefaultSchema<number | null> {
    private _maxValue?;
    private _minValue?;
    private _lessValue?;
    private _greaterValue?;
    constructor(lang?: 'fr' | 'en');
    private init;
    private initRule;
    min(minValue: number): this;
    max(maxValue: number): this;
    less(lessValue: number): this;
    greater(greaterValue: number): this;
    negative(): this;
    positive(): this;
    signed(): this;
    decimal(nber?: number | undefined): this;
    integer(): this;
    multiple(nber: number): this;
    TCPPort(): this;
}
export declare class JONStringSchema extends JONDefaultSchema<string | number | boolean | null> {
    private _lengthValue?;
    private _maxValue?;
    private _minValue?;
    private _lessValue?;
    private _greaterValue?;
    constructor(lang?: 'fr' | 'en');
    private init;
    private initRule;
    length(lengthValue: number): this;
    min(minValue: number): this;
    max(maxValue: number): this;
    less(lessValue: number): this;
    greater(greaterValue: number): this;
    regexp(ruleValue: RegExp): this;
    alphanum(): this;
    base64(paddingRequired?: boolean, urlSafe?: boolean): this;
    lowercase(): this;
    uppercase(): this;
    toLowercase(): this;
    toUppercase(): this;
    private CapitalizeAction;
    toCapitalize(): this;
    private UcFirstAction;
    toUcFirst(): this;
    creditCard(types?: ('mastercard' | 'visa' | 'american-express' | 'discover' | 'diners-club' | 'jcb')[]): this;
    dataUri(): this;
    domain(): this;
    url(): this;
    hostname(): this;
    IPAddress(types?: ('ipv4' | 'ipv6')[]): this;
    email(): this;
    guid(versions?: ('v1' | 'v2' | 'v3' | 'v4' | 'v5')[]): this;
    hexa(insensitive?: boolean): this;
    binary(): this;
    date(format?: string | undefined): this;
    identifier(): this;
}
export declare class JONBooleanSchema extends JONDefaultSchema<any> {
    private _trueValues;
    private _falseValues;
    constructor(lang?: 'fr' | 'en');
    private init;
    private initRule;
    trueValues(values: any[]): this;
    falseValues(values: any[]): this;
    default(defaultValue: any): this;
}
export declare class JONDateSchema extends JONDefaultSchema<string | number | Date | null> {
    private _maxValue?;
    private _minValue?;
    private _lessValue?;
    private _greaterValue?;
    private _defaultFormat;
    private _format;
    constructor(lang?: 'fr' | 'en');
    private init;
    private initRule;
    min(minValue: any): this;
    max(maxValue: any): this;
    less(lessValue: any): this;
    greater(greaterValue: any): this;
    format(format: string | undefined, strict?: boolean): this;
    default(defaultValue: string | number | Date | null): this;
}
export declare class JONEnumSchema extends JONDefaultSchema<any> {
    private _choices;
    constructor(lang?: 'fr' | 'en');
    private init;
    private initRule;
    choices(...values: any[]): this;
    getChoices(): any[];
}
export declare class JONNotInEnumSchema extends JONDefaultSchema<any> {
    private _choices;
    constructor(lang?: 'fr' | 'en');
    private init;
    private initRule;
    choices(...values: any[]): this;
    getChoices(): any[];
}
export declare class JONChosenTypeSchema extends JONDefaultSchema<any> {
    private _choices;
    constructor(lang?: 'fr' | 'en');
    private init;
    private initRule;
    choices(...values: any[]): this;
    getChoices(): (JONAnySchema | JONObjectSchema | JONNumberSchema | JONStringSchema | JONBooleanSchema | JONDateSchema | JONEnumSchema | JONNotInEnumSchema | JONArraySchema | JONChosenTypeSchema | JONFileSchema)[];
}
export declare class JONFileSchema extends JONDefaultSchema<any> {
    private _lengthValue?;
    private _maxValue?;
    private _minValue?;
    private _lessValue?;
    private _greaterValue?;
    private _filename?;
    private _defaultFileType;
    regexExpressionsPossibles: RegExp[];
    constructor(lang?: 'fr' | 'en');
    private init;
    private fileValueIsValidPreSUB;
    private fileValueIsValidSUB;
    private fileValueIsValid;
    private initRule;
    filename(filename: string | RegExp): this;
    type(...types: (string | RegExp)[]): this;
    length(lengthValue: number): this;
    min(minValue: number): this;
    max(maxValue: number): this;
    less(lessValue: number): this;
    greater(greaterValue: number): this;
    changeFileType(type: any): this;
}
export declare class JONClone {
    private _target;
    constructor(target: string, lang?: 'fr' | 'en');
    get target(): string | undefined;
    set target(target: string | undefined);
}
export declare class JONRef {
    private _target;
    constructor(target: string, lang?: 'fr' | 'en');
    get target(): string | undefined;
    set target(target: string | undefined);
}
export {};
