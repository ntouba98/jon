import {
    Blob,
} from 'buffer';
import Config from './config.json';
import {
    JONRule,
    JONRes,
} from './interface';
import Timez from '@hivi/timez/timez';

function isBlob(value: any) {
    return (
        !!value &&
        value != null &&
        value != undefined &&
        typeof value === 'object' &&
        Array.isArray(value) === false &&
        'size' in value &&
        'type' in value &&
        'arrayBuffer' in value &&
        'slice' in value &&
        'stream' in value &&
        'text' in value
    )
}
function isFile(value: any) {
    return (
        !!value &&
        value != null &&
        value != undefined &&
        typeof value === 'object' &&
        Array.isArray(value) === false &&
        'size' in value &&
        'type' in value &&
        'arrayBuffer' in value &&
        'slice' in value &&
        'stream' in value &&
        'text' in value &&
        'lastModified' in value &&
        'name' in value &&
        'webkitRelativePath' in value
    )
}

class JONDefaultSchema<T = any > {
    protected _label: string | undefined;
    protected _lang: 'fr' | 'en' = 'en';
    protected _options: {
        validationType: (
            'number' |
            'boolean' |
            'string' |
            'object' |
            'list' |
            'date' |
            'enum' |
            'file' |
            'any'
        ),
        type: (
            'number' |
            'boolean' |
            'string' |
            'object' |
            'list' |
            'date' |
            'enum' |
            'file' |
            'any'
        ),
        instance?: (
            Date |
            Blob
        ),
        rules: JONRule[],
    } = {
        validationType: 'number',
        type: 'number',
        instance: undefined,
        rules: [],
    };
    protected _finalError = new Error({
        fr: 'unknown error 2',
        en: 'erreur inconnue 2',
    }[this._lang]);
    protected _customError: Error | undefined;
    protected _params: {} = {};
    protected _defaultValue: T | undefined;
    private _isUndefined: boolean = false;
    private _map: ((value: T) => T) | undefined = undefined

    constructor(lang: 'fr' | 'en' = 'en') {
        this.lang = lang;
    }

    public options() {
        return this._options;
    }

    private validator(
        value: any,
    ): JONRes {
        let data: any | undefined;
        let valid: boolean = false;
        let error: Error | undefined;

        // console.log(`> @hivi.JON.schemas | JONDefaultSchema - validator - this._options.validationType:: `, this._options.validationType);
        if(!['number', 'string', 'boolean', 'date', 'enum', 'object', 'list', 'any', 'file'].includes(this._options.validationType)) {
            return {
                data,
                valid,
                error: Error({
                    fr: 'unknown error',
                    en: 'erreur inconnue',
                }[this._lang]),
            };
        }

        if(Config.debug) {
            // console.log(`> @hivi.JON.schemas | JONDefaultSchema - validator - this._options.rules:: `, this._options.rules);
        }
        for (let indexRules = 0; indexRules < this._options.rules.length; indexRules++) {
            const ruleData = this._options.rules[indexRules];
            if(ruleData.init) {
                ruleData.init(value);
            }
        }
        for (let indexRules = 0; indexRules < this._options.rules.length; indexRules++) {
            const ruleData = this._options.rules[indexRules];
            value = (indexRules === 0 ? value : data);
            // console.log(`> @hivi.JON.schemas | JONDefaultSchema - value(old):: `, value);
            if(
                !(
                    !!value ||
                    typeof value === 'boolean' ||
                    (this.getIsUndefined() && !value && typeof value !== 'boolean') ||
                    value === 0
                )
            ) {
                value = this._defaultValue;
            }
            // console.log(`> @hivi.JON.schemas | JONDefaultSchema - value:: `, value);
            const res = (!!ruleData.rule) ? ruleData.rule(value) : (
                (value: any) => ({
                    data: value,
                    valid: true,
                    error: undefined,
                })
            )(value);

            data = ruleData.sanitize ? ruleData.sanitize(res) : res.data;
            valid = res.valid;
            error = (this._customError && !res.valid) ? this._customError : res.error;

            if(!valid) {
                break;
            }
        }

        if(!!valid && !!this._map) {
            data = this._map(data)
        }

        return {
            data,
            valid,
            error,
        };
    }

    protected get rules() {
        return this._options.rules;
    }
    protected set rules(rules: any) {
        this._options.rules = rules;
    }
    public get lang() {
        return this._lang;
    }
    public set lang(lang: 'fr' | 'en') {
        this._lang = lang;
    }
    
    protected getIsUndefined() {
        return this._isUndefined;
    }
    public isUndefined() {
        this._isUndefined = true;

        return this;
    }
    public getLabel() {
        return this._label;
    }
    public setLabel(label: string) {
        this._label = label;
        return this;
    }
    protected getError() {
        return this._finalError;
    }
    protected setFinalError(errorValue: any) {
        // console.log('> error: ', errorValue);
        this._finalError = new Error(
            (
                typeof errorValue === 'object' &&
                Array.isArray(errorValue) === false
            ) ? errorValue[this._lang] : errorValue
        );
    }
    public setError(errorValue: any) {
        this._customError = new Error(
            (
                typeof errorValue === 'object' &&
                Array.isArray(errorValue) === false
            ) ? errorValue[this._lang] : errorValue
        );

        return this;
    }
    protected ruleNames = () => this._options.rules.map((rule) => rule.name);
    protected addOtherRules(
        rules: JONRule[],
    ) {
        this._options.rules = [
            ...this._options.rules,
            ...rules,
        ]
    }

    public required() {
        /* verifier si la valeur est requise ou non */
        const ruleData = {
            name: 'required',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    !([undefined, null, NaN].includes(value)) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} est requis`,
                        en: `${labelSTR} is required`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public default(defaultValue: T) {
        this._defaultValue = defaultValue;
        return this;
    }
    public getDefault() {
        return this._defaultValue;
    }

    public error(value: any): Error | undefined {
        return this.validate(value).error;
    }
    public isValid(value: any): boolean {
        return this.validate(value).valid;
    }
    public sanitize(value: any): any | undefined {
        return this.validate(value).data;
    }
    public validate(value: any): JONRes {
        if(Config.debug) {
            // console.log(`> @hivi.JON.schemas | JONDefaultSchema - validate - this._options.type:: `, this._options.type);
            // console.log(`> @hivi.JON.schemas | JONDefaultSchema - validate - this._options.instance:: `, this._options.instance);
        }

        if(['number', 'string', 'boolean', 'date', 'enum', 'object', 'list', 'any', 'file'].includes(this._options.validationType)) {
            return this.validator(value);
        }

        return {
            data: undefined,
            valid: false,
            error: this.getError(),
        };
    }
    
    public enum(...choices: T[]) {
        /* verifier si l'element correspond à un des choix défini ci-dessus */
        choices = (
            typeof choices === 'object' &&
            Array.isArray(choices) === true &&
            choices.length > 0
        ) ? choices : [];
        const ruleData = {
            name: 'enum',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            choices.includes(value)
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `ne correspond à aucun choix défini`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `does not correspond to any defined choice`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public enumNot(...choices: T[]) {
        /* verifier si l'element ne doit pas correspond à un des choix défini ci-dessus */
        choices = (
            typeof choices === 'object' &&
            Array.isArray(choices) === true &&
            choices.length > 0
        ) ? choices : [];
        const ruleData = {
            name: 'enum',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            !choices.includes(value)
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `ne doit pas correspondre à l'un des choix predefinis`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `must not match one of the predefined choices`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }

    protected checkIsUndefined(value: any) {
        return value == null || (!value && typeof value != 'boolean') || (this.getIsUndefined() && (value === undefined || value == null));
    }

    
    public applyApp(
        name: string = 'applyApp',
        init: (value: any) => any = (value: any) => value,
        rule: ((value: any) => JONRes) | undefined = (value: any) => ({
            data: value,
            valid: true,
            error: undefined,
        }),
        sanitize: (value: JONRes) => any = (value: JONRes) => value as any,
    ) {
        this._options.rules.push({
            name,
            init,
            rule,
            sanitize,
        });

        return this;
    }
    public applyMapping(
        mapAction: ((value: T) => T),
    ) {
        this._map = mapAction;

        return this;
    }
}

export class JONAnySchema extends JONDefaultSchema<any> {
    constructor(lang: 'fr' | 'en' = 'fr') {
        super(lang);

        this.init();
    }

    private init() {
        this._options.validationType = 'any';
        this._options.type = 'any';
        this.initRule();
    }

    private initRule() {
        // first rule
        const firstRule: JONRule = {
            name: 'init',
            sanitize: (value: JONRes) => {
                if(this.getIsUndefined() && !value.data && typeof value.data != 'boolean') {
                    return undefined
                }
                return value.data;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = true;
                let error: Error | undefined;
    
                valid = (
                    ['object', 'number', 'string', 'boolean'].includes(typeof value) ||
                    isFile(value) ||
                    isBlob(value) ||
                    (this.getIsUndefined() && !value && typeof value !== 'boolean') ||
                    value instanceof Date
                );

                if(Config.debug) {
                    // console.log(`> @hivi.JON.schemas | JONObjectSchema - initRule - value:: `, value);
                    // console.log(`> @hivi.JON.schemas | JONObjectSchema - initRule - valid:: `, valid);
                }
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}" ` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];

                    this.setFinalError({
                        fr: `${labelSTR} est invalide`,
                        en: `${labelSTR} is invalid`,
                    }[this._lang]);

                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(firstRule);
    }
}
export class JONObjectSchema extends JONDefaultSchema<any> {
    private _struct: any = {};
    private _primaryStruct: boolean = false;
    private _links: {
        target: string,
        item: string
    }[] = [];
    private _nolinks: {
        target: string,
        item: string
    }[] = [];
    private _lengthValue?: number = undefined;
    private _maxValue?: number = undefined;
    private _minValue?: number = undefined;
    private _lessValue?: number = undefined;
    private _greaterValue?: number = undefined;
    constructor(lang: 'fr' | 'en' = 'fr') {
        super(lang);

        this.init();
    }

    private init() {
        this._options.validationType = 'object';
        this._options.type = 'object';
        this.initRule();
    }
    private initRule() {
        // first rule
        const JSONGetInvalidItems = (value: any) => {
            if(
                !(
                    (
                        typeof value === 'object' &&
                        Array.isArray(value) === false
                    ) && (
                        typeof this._struct === 'object' &&
                        Array.isArray(this._struct) === false
                    )
                ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
            ) {
                return Object.fromEntries(
                    new Map([])
                );
            }

            const labelSTR = this.getLabel() ? `"${this.getLabel()}" ` : {
                fr: 'l\'element',
                en: 'the element'
            }[this._lang];

            const res: any = Object.fromEntries(
                new Map(
                    Object.keys(this._struct)
                    .map((key: string, index: number) => ([key, this._struct[key]]))
                    .map((val: any[], index: number) => {
                        const type = val[1];
                        const label = type.getLabel();
                        type.setLabel(label || `${labelSTR}.${val[0]}`)
                        val[1] = val[1].validate(value[val[0]]);
                        return val;
                    }).filter((val: any[], index: number) => !val[1].valid) as any[]
                )
            );
            const cmpStructs = (struct1: any, struct2: any) => (
                Object.fromEntries(
                    new Map(
                        Object.keys(struct1).filter(
                            (key: string) => !(
                                Object.keys(struct2).includes(key)
                            )
                        ).map((key: string, index: number) => [key, struct1[key]])
                     )
                )
            );
            const res2a = cmpStructs(this._struct, value);
            let res2b = (Object.keys(this._struct).length > 0) ? cmpStructs(value, this._struct) : {};
            res2b = (this._primaryStruct) ? {} : res2b;

            // console.log(`> @hivi.JON.schemas - JONObjectSchema - initRule - JSONGetInvalidItems | Object.keys(res2a).length:: `, Object.keys(res2a).length);
            // console.log(`> @hivi.JON.schemas - JONObjectSchema - initRule - JSONGetInvalidItems | Object.keys(res2b).length:: `, Object.keys(res2b).length);
            // console.log(`> @hivi.JON.schemas - JONObjectSchema - initRule - JSONGetInvalidItems | res2b:: `, res2b);

            let res2: any = ((Object.keys(res2a).length > 0) ? res2a : res2b);
            res2 = Object.fromEntries(
                new Map(
                    Object.keys(res2).map(
                        (key: string, index: number) => [
                            key,
                            ({
                                data: undefined,
                                valid: false,
                                error: new Error({
                                    fr: `${labelSTR} a une structure invalide`,
                                    en: `${labelSTR} has an invalid structure`,
                                }[this._lang]),
                            }),
                        ]
                    )
                )
            );
            const res3: any = Object.fromEntries(
                new Map(
                    this._links.filter((link: {
                        target: string;
                        item: string;
                    }, index: number) => !(
                        value[link.target] === value[link.item]
                    )).map((link: {
                        target: string;
                        item: string;
                    }, index: number) => [link.item, {
                        data: undefined,
                        valid: false,
                        error: new Error({
                            fr: `${labelSTR}.${link.item} et ${labelSTR}.${link.target} ont une valeur differente`,
                            en: `${labelSTR}.${link.item} and ${labelSTR}.${link.target} have different value`,
                        }[this._lang])
                    }])
                )
            );
            const res4: any = Object.fromEntries(
                new Map(
                    this._nolinks.filter((nolink: {
                        target: string;
                        item: string;
                    }, index: number) => !(
                        value[nolink.target] !== value[nolink.item]
                    )).map((nolink: {
                        target: string;
                        item: string;
                    }, index: number) => [nolink.item, {
                        data: undefined,
                        valid: false,
                        error: new Error({
                            fr: `${labelSTR}.${nolink.item} et ${labelSTR}.${nolink.target} n'ont pas une valeur différente`,
                            en: `${labelSTR}.${nolink.item} and ${labelSTR}.${nolink.target} dont have different value`,
                        }[this._lang])
                    }])
                )
            );

            // console.log(`> @hivi.JON.schemas - JONObjectSchema - initRule - JSONGetInvalidItems | res3.confirm_password.error:: `, res3.confirm_password.error.message);
            // console.log(`> @hivi.JON.schemas - JONObjectSchema - initRule - JSONGetInvalidItems | res:: `, res);
            // console.log(`> @hivi.JON.schemas - JONObjectSchema - initRule - JSONGetInvalidItems | res3:: `, res3);
            // console.log(`> @hivi.JON.schemas - JONObjectSchema - initRule - JSONGetInvalidItems | res4:: `, res4);

            if(Object.keys(res2).length > 0) {
                return res2;
            } else if(Object.keys(res3).length > 0) {
                return res3;
            } else if(Object.keys(res4).length > 0) {
                return res4;
            }

            return res;
        };
        const JSONIsValid = (value: any) => {
            if((this.getIsUndefined() && !value && typeof value !== 'boolean')) {
                return true;
            }
            const invalidDatas = JSONGetInvalidItems(value);
            const res: boolean = (
                (
                    typeof value === 'object' &&
                    Array.isArray(value) === false
                ) &&
                (
                    typeof this._struct === 'object' &&
                    Array.isArray(this._struct) === false
                ) &&
                Object.keys(invalidDatas).length <= 0
            );

            return res;
        }
        const cleanValue = (value: any) => {
            for (let index = 0; index < Object.keys(this._struct).length; index++) {
                const key = Object.keys(this._struct)[index];
                const element = this._struct[key];
                
                if(checkIfCorrectTypeSchema(element)) {
                    if(
                        !(
                            !value ||
                            value[key] ||
                            typeof(value[key]) === 'boolean' ||
                            value[key] === null
                        )
                    ) {
                        value[key] = element.getDefault();
                    }
                }
            }

            return value;
        };
        const firstRule: JONRule = {
            name: 'init',
            sanitize: (value: JONRes) => {
                if(this.getIsUndefined() && !value.data && typeof value.data != 'boolean') {
                    return undefined
                }
                // console.log(`> @hivi.JON.schemas | JONObjectSchema - sanitize - value.data:: `, value.data);
                // console.log(`> @hivi.JON.schemas | JONObjectSchema - sanitize - JSONIsValid(value.data):: `, JSONIsValid(value.data));
                // console.log(`> @hivi.JON.schemas | JONObjectSchema - sanitize - this._struct:: `, this._struct);
                let finalValueData: any = undefined;
                const oldValue = {
                    ...{
                        value: value.data
                    }
                }.value;
                
                const mergePrimaryAndSecondaryData = (oldValue: any, newValue: any) => {
                    // recuperer tous les elements qui ne sont pas dans la structure primaire
                    if(this._primaryStruct && !!oldValue && !!newValue) {
                        oldValue = (
                            typeof oldValue === 'object' &&
                            Array.isArray(oldValue) === false
                        ) ? oldValue : {};
                        newValue = (
                            typeof newValue === 'object' &&
                            Array.isArray(newValue) === false
                        ) ? newValue : {};

                        const secondaryStruct_key: string[] = Object.keys(oldValue).filter((key: string, index: number) => !Object.keys(this._struct).includes(key));
                        const secondaryStruct: any = Object.fromEntries(
                            new Map(
                                secondaryStruct_key.map((key: string, index: number) => [key, new JONAnySchema(this._lang)])
                            )
                        );
                        const secondaryData: any = Object.fromEntries(
                            new Map(
                                Object.keys(secondaryStruct).map((key: string, index: number) => [key, secondaryStruct[key]])
                                .filter((val: any[], index: number) => val[1].validate(oldValue[val[0]]).valid)
                                .map((val: any[], index: number) => [
                                    val[0],
                                    val[1].validate(oldValue[val[0]]).data
                                ]) as any[]
                            )
                        );
                        // console.log(`> @hivi.JON.schemas | JONObjectSchema - sanitize - secondaryData:: `, secondaryData);
                        newValue = {
                            ...newValue,
                            ...secondaryData,
                        };
                    }

                    return newValue;
                };
                
                if(JSONIsValid(value.data)) {
                    if(value.data !== null) {
                        if(Object.keys(this._struct).length > 0) {
                            const resPart1: any[] = Object.keys(this._struct).map(
                                (key: string, index: number) => [
                                    key,
                                    this._struct[key].sanitize(value.data[key])
                                ]
                            );
                            const resPart2 = new Map(
                                resPart1
                            );
                            const res = Object.fromEntries(
                                resPart2
                            );
                            // console.log(`> @hivi.JON.schemas | JONObjectSchema - sanitize - resPart1:: `, resPart1);
                            // console.log(`> @hivi.JON.schemas | JONObjectSchema - sanitize - resPart2:: `, resPart2);
                            // console.log(`> @hivi.JON.schemas | JONObjectSchema - sanitize - res:: `, res);
    
                            finalValueData = res;
                        } else {
                            finalValueData = value.data;
                        }
                    } else {
                        finalValueData = null;
                    }
                } else {
                    finalValueData = undefined;
                }

                const finalResult = mergePrimaryAndSecondaryData(oldValue, finalValueData);

                // console.log(`> @hivi.JON.schemas | JONObjectSchema - sanitize - value:: `, value);
                // console.log(`> @hivi.JON.schemas | JONObjectSchema - sanitize - oldValue:: `, oldValue);
                // console.log(`> @hivi.JON.schemas | JONObjectSchema - sanitize - finalValueData:: `, finalValueData);
                // console.log(`> @hivi.JON.schemas | JONObjectSchema - sanitize - finalResult:: `, finalResult);

                return finalResult;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
                
                if(Config.debug) {
                    console.log(`> @hivi.JON.schemas | JONObjectSchema - initRule - value (OLD):: `, value);
                }
                value = cleanValue(value);
                valid = (JSONIsValid(value) || (this.getIsUndefined() && !value && typeof value !== 'boolean'));

                if(Config.debug) {
                    // console.log(`> @hivi.JON.schemas | JONObjectSchema - initRule - value:: `, value);
                    // console.log(`> @hivi.JON.schemas | JONObjectSchema - initRule - valid:: `, valid);
                }
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const invalidsItems = JSONGetInvalidItems(value);
                    // console.log(`> @hivi.JON.schemas | JONObjectSchema - initRule - invalidsItems:: `, invalidsItems);
                    // console.log(`> @hivi.JON.schemas | JONObjectSchema - initRule - Object.keys(invalidsItems).length:: `, Object.keys(invalidsItems).length);
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}" ` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(Object.keys(invalidsItems).length <= 0) {
                        this.setFinalError({
                            fr: `${labelSTR} est invalide`,
                            en: `${labelSTR} is invalid`,
                        }[this._lang]);
                    } else {
                        this.setFinalError((Object.values(invalidsItems)[0] as any).error.message);
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(firstRule);
    }
    public length(lengthValue: number) {
        /* verifier si la taille de l'objet est égale à "lengthValue" */
        const ruleData = {
            name: 'length',
            init: (value: any) => {
                this._lengthValue = lengthValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof value === 'object' &&
                            Array.isArray(value) === false &&
                            Object.keys(value).length === lengthValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `la taille de ${labelSTR} doit être égale ${lengthValue}`,
                        en: `the size of ${labelSTR} must be equal ${lengthValue}`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public min(minValue: number) {
        /* verifier si la taille de l'objet est supérieure ou égal à "minValue" */
        const ruleData = {
            name: 'min',
            init: (value: any) => {
                this._minValue = minValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof value === 'object' &&
                            Array.isArray(value) === false &&
                            Object.keys(value).length >= minValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('max')
                    ) {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être comprise entre ${minValue} et ${this._maxValue}`,
                            en: `the size of ${labelSTR} must be between ${minValue} and ${this._maxValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être au minimum ${minValue}`,
                            en: `the size of ${labelSTR} must be at least ${minValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public max(maxValue: number) {
        /* verifier si la taille de l'objet est inférieure ou égal à "maxValue" */
        const ruleData = {
            name: 'max',
            init: (value: any) => {
                this._maxValue = maxValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                this._maxValue = maxValue;
    
                valid = (
                    (
                        (
                            typeof value === 'object' &&
                            Array.isArray(value) === false &&
                            Object.keys(value).length <= maxValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('min')
                    ) {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être comprise entre ${this._minValue} et ${maxValue}`,
                            en: `the size of ${labelSTR} must be between ${this._minValue} and ${maxValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être au maximum ${maxValue}`,
                            en: `the size of ${labelSTR} must be maximum ${maxValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public less(lessValue: number) {
        /* verifier si la taille de l'objet est supérieure à "lessValue" */
        const ruleData = {
            name: 'less',
            init: (value: any) => {
                this._lessValue = lessValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof value === 'object' &&
                            Array.isArray(value) === false &&
                            Object.keys(value).length > lessValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('greater')
                    ) {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être supérieure à ${lessValue} et inférieure à ${this._greaterValue}`,
                            en: `the size of ${labelSTR} must be greater than ${lessValue} and less than ${this._greaterValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être supérieure à ${lessValue}`,
                            en: `the size of ${labelSTR} must be greater than ${lessValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public greater(greaterValue: number) {
        /* verifier si la taille de l'objet' est inférieure à "greaterValue" */
        const ruleData = {
            name: 'greater',
            init: (value: any) => {
                this._greaterValue = greaterValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                this._greaterValue = greaterValue;
    
                valid = (
                    (
                        (
                            typeof value === 'object' &&
                            Array.isArray(value) === false &&
                            Object.keys(value).length < greaterValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('less')
                    ) {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être supérieure à ${this._lessValue} et inférieure à ${greaterValue}`,
                            en: `the size of ${labelSTR} must be greater than ${this._lessValue} and less than ${greaterValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être inférieure à ${greaterValue}`,
                            en: `the size of ${labelSTR} must be less than ${greaterValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }

    public requiredAttributes(...attributes: string[]) {
        /* verifier si l'objet contient tous les attributs requis */
        const ruleData = {
            name: 'requiredAttributes',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof value === 'object' &&
                            Array.isArray(value) === false &&
                            Object.keys(value).filter((key: string, index: number) => attributes.includes(key)).length > 0 &&
                            Object.keys(value).filter((key: string, index: number) => attributes.includes(key)).length === attributes.length
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];

                    this.setFinalError({
                        fr: `${labelSTR} ne contient pas tous les attributs requis`,
                        en: `${labelSTR} does not contain all required attributes`,
                    });

                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public flexibleAttributes(...attributes: string[]) {
        /* verifier si l'objet contient l'un des attributs requis */
        const ruleData = {
            name: 'flexibleAttributes',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof value === 'object' &&
                            Array.isArray(value) === false &&
                            Object.keys(value).filter((key: string, index: number) => attributes.includes(key)).length > 0 &&
                            Object.keys(value).filter((key: string, index: number) => attributes.includes(key)).length <= attributes.length
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];

                    this.setFinalError({
                        fr: `${labelSTR} doit contenir l'un des attributs requis`,
                        en: `${labelSTR} must contain one of the required attributes`,
                    });

                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }

    public struct(values: any) {
        values = (
            typeof values === 'object' &&
            Array.isArray(values) === false
        ) ? values : {};
        let subValues: any[] = Object.keys(values).filter(
            (key: string, index: number) => checkIfCorrectTypeSchema(values[key]) ||
            (
                values[key] instanceof JONClone &&
                !!(values[key] as JONClone).target &&
                Object.keys(values).includes(
                    String((values[key] as JONClone).target)
                )
            ) ||
            (
                values[key] instanceof JONRef &&
                !!(values[key] as JONRef).target &&
                Object.keys(values).includes(
                    String((values[key] as JONRef).target)
                )
            )
        ).map(
            (key: string, index: number) => [key, values[key]]
        );

        // clones
        let allClones: ({
            key: any;
            value: JONClone;
        })[] = subValues
        .filter((value: any, index: number) => (value[1] instanceof JONClone))
        .map((value: any, index: number) => (
            {
                key: value[0],
                value: value[1] as JONClone,
            }
        ));
        allClones.forEach((ref: ({
            key: any;
            value: JONClone;
        })) => {
            subValues.push([
                ref.key,
                subValues.find((value: any, index: number) => value[0] === ref.value.target)[1],
            ]);
        });
        //
        subValues = subValues.filter((value: any) => (
            !(value[1] instanceof JONClone)
        ));

        // links
        let allLinks: ({
            key: any;
            value: JONRef;
        })[] = subValues
        .filter((value: any, index: number) => (value[1] instanceof JONRef))
        .map((value: any, index: number) => (
            {
                key: value[0],
                value: value[1] as JONRef,
            }
        ));
        allLinks.forEach((link: ({
            key: any;
            value: JONRef;
        }), index: number) => {
            subValues.push([
                link.key,
                subValues.find((value: any, index: number) => value[0] === link.value.target)[1],
            ]);
        });
        //
        subValues = subValues.filter((value: any) => (
            !(value[1] instanceof JONRef)
        ));

        this._struct = Object.fromEntries(
            new Map(
                subValues
            )
        );

        // links (suite)
        allLinks.forEach((link: ({
            key: any;
            value: JONRef;
        }), index: number) => {
            if(link.value.target) {
                this.link(link.value.target, link.key);
            }
        });

        // console.log(`> @hivi.JON.schemas - JONObjectSchema - struct | this._links:: `, this._links);
        // console.log(`> @hivi.JON.schemas - JONObjectSchema - struct | allClones:: `, allClones);
        // console.log(`> @hivi.JON.schemas - JONObjectSchema - struct | checkIfCorrectTypeSchema(values.nom):: `, checkIfCorrectTypeSchema(values.nom));
        // console.log(`> @hivi.JON.schemas - JONObjectSchema - struct | subValues:: `, subValues);
        // console.log(`> @hivi.JON.schemas - JONObjectSchema - struct | this._struct:: `, this._struct);

        return this;
    }
    public primaryStruct(values: any) {
        this._primaryStruct = true;

        return this.struct(values);
    }
    public getStruct() {
        return this._struct;
    }
    public link(target: string, item: string) {
        if(
            Object.keys(this._struct).includes(target) &&
            Object.keys(this._struct).includes(item)
        ) {
            this._links.push({
                target,
                item,
            });
        }

        return this;
    }
    public nolink(target: string, item: string) {
        if(
            Object.keys(this._struct).includes(target) &&
            Object.keys(this._struct).includes(item)
        ) {
            this._nolinks.push({
                target,
                item,
            });
        }

        return this;
    }
}
export class JONArraySchema extends JONDefaultSchema<any[] | null> {
    private _types: (
        JONNumberSchema |
        JONStringSchema |
        JONBooleanSchema |
        JONDateSchema |
        JONEnumSchema |
        JONNotInEnumSchema |
        JONObjectSchema |
        JONArraySchema |
        JONChosenTypeSchema |
        JONAnySchema |
        JONFileSchema
    )[] = [].filter(checkIfCorrectTypeSchema).map(convertInCorrectSchemaType);
    private _lengthValue?: number = undefined;
    private _maxValue?: number = undefined;
    private _minValue?: number = undefined;
    private _lessValue?: number = undefined;
    private _greaterValue?: number = undefined;

    constructor(lang: 'fr' | 'en' = 'fr') {
        super(lang);

        this.init();
    }

    private init() {
        this._options.validationType = 'list';
        this._options.type = 'list';
        this.initRule();
    }
    private initRule() {
        // first rule
        const invalidArrayItem = (values: any) => {
            if(
                !(
                    typeof values === 'object' &&
                    Array.isArray(values) === true
                )
            ) {
                return [];
            }
            const labelSTR = this.getLabel() ? `"${this.getLabel()}" ` : {
                fr: 'l\'element',
                en: 'the element'
            }[this._lang];
            return (
                (values as any[]).map((value: any, index: number) => ({
                    index: index,
                    value: value,
                }))
                .filter((value: ({
                    index: number;
                    value: any;
                }), index: number) => !(
                    this._types.filter(
                        (
                            type: (
                                JONNumberSchema |
                                JONStringSchema |
                                JONBooleanSchema |
                                JONDateSchema |
                                JONEnumSchema |
                                JONNotInEnumSchema |
                                JONObjectSchema |
                                JONArraySchema |
                                JONChosenTypeSchema |
                                JONAnySchema |
                                JONFileSchema
                            )
                        ) => type.isValid(value.value)
                    ).length > 0
                )).map((value: ({
                    index: number;
                    value: any;
                }), index: number) => {
                    const type = this._types.find(
                        (type) => !type.isValid(value.value)
                    );
                    const label = type?.getLabel();
                    type?.setLabel(label || `${labelSTR}[${index}]`);
                    const newValue = type?.validate(value.value);
                    return {
                        index,
                        value: newValue,
                    };
                })
            )
        };
        const arrayIsInvalid = (values: any) => (
            (
                typeof values === 'object' &&
                Array.isArray(values) === true
            ) && invalidArrayItem(values).length <= 0
        );
        const cleanValues = (values: any) => {
            if(
                typeof values === 'object' &&
                Array.isArray(values) === true &&
                invalidArrayItem(values).length <= 0
            ) {
                values = (values as any[])
                .map((value: any, index: number) => {
                    const type = this._types.find(
                        (
                            type: (
                                JONNumberSchema |
                                JONStringSchema |
                                JONBooleanSchema |
                                JONDateSchema |
                                JONEnumSchema |
                                JONNotInEnumSchema |
                                JONObjectSchema |
                                JONArraySchema |
                                JONChosenTypeSchema |
                                JONAnySchema |
                                JONFileSchema
                            )
                        ) => type.isValid(value)
                    );
                    if(
                        type &&
                        checkIfCorrectTypeSchema(type)
                    ) {
                        if(
                            !(
                                value ||
                                typeof(value) === 'boolean' ||
                                (this.getIsUndefined() && !value && typeof value !== 'boolean')
                            )
                        ) {
                            value = type.getDefault();
                        }
                    }

                    return value;
                })
            }
            return values;
        };
        const firstRule: JONRule = {
            name: 'init',
            sanitize: (value: JONRes) => {
                if(this.getIsUndefined() && !value.data && typeof value.data != 'boolean') {
                    return undefined
                }
                if(arrayIsInvalid(value.data)) {
                    if(value.data !== null) {
                        if(this._types.length > 0) {
                            return (value.data as any[]).map(
                                (dt: any) => this._types.find(
                                    (type) => type.isValid(dt)
                                )?.sanitize(dt)
                            );
                        } else {
                            return value.data;
                        }
                    } else {
                        return null;
                    }
                } else {
                    return undefined;
                }
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                value = cleanValues(value);
                valid = (arrayIsInvalid(value) || (this.getIsUndefined() && !value && typeof value !== 'boolean'));
                if(Config.debug) {
                    console.log(`> @hivi.JON.schemas | JONArraySchema - initRule - value:: `, value);
                    console.log(`> @hivi.JON.schemas | JONArraySchema - initRule - valid:: `, valid);
                    console.log(`> @hivi.JON.schemas | JONArraySchema - initRule - this.getIsUndefined():: `, this.getIsUndefined());
                    console.log(`> @hivi.JON.schemas | JONArraySchema - initRule - this.getIsUndefined():: `, this.getIsUndefined());
                }
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const invalidItems = invalidArrayItem(value);
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}" ` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(invalidItems.length <= 0) {
                        this.setFinalError({
                            fr: `${labelSTR} est d'un type invalide`,
                            en: `${labelSTR} is of an invalid type`,
                        });
                    } else {
                        if(
                            invalidItems[0].value &&
                            invalidItems[0].value.error
                        ) {
                            this.setFinalError(invalidItems[0].value.error.message)
                        } else {
                            this.setFinalError({
                                fr: `${labelSTR}[${invalidItems[0].index}] est d'un type invalide`,
                                en: `${labelSTR}[${invalidItems[0].index}] is of an invalid type`,
                            });
                        }
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(firstRule);
    }
    public length(lengthValue: number) {
        /* verifier si la taille de la liste est égale à "lengthValue" */
        const ruleData = {
            name: 'length',
            init: (value: any) => {
                this._lengthValue = lengthValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof value === 'object' &&
                            Array.isArray(value) === true &&
                            Object.keys(value).length === lengthValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `la taille de ${labelSTR} doit être égale ${lengthValue}`,
                        en: `the size of ${labelSTR} must be equal ${lengthValue}`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public min(minValue: number) {
        /* verifier si la taille de la liste est supérieure ou égal à "minValue" */
        const ruleData = {
            name: 'min',
            init: (value: any) => {
                this._minValue = minValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof value === 'object' &&
                            Array.isArray(value) === true &&
                            Object.keys(value).length >= minValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('max')
                    ) {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être comprise entre ${minValue} et ${this._maxValue}`,
                            en: `the size of ${labelSTR} must be between ${minValue} and ${this._maxValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être au minimum ${minValue}`,
                            en: `the size of ${labelSTR} must be at least ${minValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public max(maxValue: number) {
        /* verifier si la taille de la liste est inférieure ou égal à "maxValue" */
        const ruleData = {
            name: 'max',
            init: (value: any) => {
                this._maxValue = maxValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                this._maxValue = maxValue;
    
                valid = (
                    (
                        (
                            typeof value === 'object' &&
                            Array.isArray(value) === true &&
                            Object.keys(value).length <= maxValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('min')
                    ) {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être comprise entre ${this._minValue} et ${maxValue}`,
                            en: `the size of ${labelSTR} must be between ${this._minValue} and ${maxValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être au maximum ${maxValue}`,
                            en: `the size of ${labelSTR} must be maximum ${maxValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public less(lessValue: number) {
        /* verifier si la taille de la liste est supérieure à "lessValue" */
        const ruleData = {
            name: 'less',
            init: (value: any) => {
                this._lessValue = lessValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof value === 'object' &&
                            Array.isArray(value) === true &&
                            Object.keys(value).length > lessValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('greater')
                    ) {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être supérieure à ${lessValue} et inférieure à ${this._greaterValue}`,
                            en: `the size of ${labelSTR} must be greater than ${lessValue} and less than ${this._greaterValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être supérieure à ${lessValue}`,
                            en: `the size of ${labelSTR} must be greater than ${lessValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public greater(greaterValue: number) {
        /* verifier si la taille de la liste' est inférieure à "greaterValue" */
        const ruleData = {
            name: 'greater',
            init: (value: any) => {
                this._greaterValue = greaterValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                this._greaterValue = greaterValue;
    
                valid = (
                    (
                        (
                            typeof value === 'object' &&
                            Array.isArray(value) === true &&
                            Object.keys(value).length < greaterValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('less')
                    ) {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être supérieure à ${this._lessValue} et inférieure à ${greaterValue}`,
                            en: `the size of ${labelSTR} must be greater than ${this._lessValue} and less than ${greaterValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être inférieure à ${greaterValue}`,
                            en: `the size of ${labelSTR} must be less than ${greaterValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }

    public types(...values: any[]) {
        this._types = (
            typeof values === 'object' &&
            Array.isArray(values) === true &&
            values.length > 0
        ) ? values.filter(checkIfCorrectTypeSchema) : [
            new JONAnySchema(this._lang)
        ];

        return this;
    }
    public getTypes() {
        return this._types;
    }

    public default(defaultValue: any[]) {
        this._defaultValue = (
            typeof defaultValue === 'object' &&
            Array.isArray(defaultValue) === true
        ) ? defaultValue : [];
        return this;
    }
}
export class JONNumberSchema extends JONDefaultSchema<number | null> {
    private _maxValue?: number = undefined;
    private _minValue?: number = undefined;
    private _lessValue?: number = undefined;
    private _greaterValue?: number = undefined;
    constructor(lang: 'fr' | 'en' = 'fr') {
        super(lang);

        this.init();
    }

    private init() {
        this._options.validationType = 'number';
        this._options.type = 'number';
        this.initRule();
    }
    private initRule() {
        // first rule
        const cleanValue = (value: any) => {
            if(
                !(
                    value ||
                    typeof(value) === 'boolean' ||
                    (this.getIsUndefined() && !value && typeof value !== 'boolean')
                )
            ) {
                value = this._defaultValue;
            }

            return value;
        }
        const firstRule: JONRule = {
            name: 'init',
            sanitize: (value: JONRes) => {
                if(this.getIsUndefined() && !value.data && typeof value.data != 'boolean') {
                    return undefined
                }
                return (value.data !== null) ? parseFloat(value.data) : null;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                value = cleanValue(value);
                valid = (
                    (
                        typeof value === this._options.type ||
                        (
                            typeof value === 'string' && (
                                !isNaN(parseInt(value)) ||
                                !isNaN(parseFloat(value))
                            )
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    )
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} est d'un type invalide`,
                        en: `${labelSTR} is of an invalid type`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(firstRule);
    }
    public min(minValue: number) {
        /* verifier si le nombre est inferieur ou égal à "minValue" */
        const ruleData = {
            name: 'min',
            init: (value: any) => {
                this._minValue = minValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (parseFloat(value) >= minValue ||
                        (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init'))
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('max')
                    ) {
                        this.setFinalError({
                            fr: `${labelSTR} doit être comprise entre ${minValue} et ${this._maxValue}`,
                            en: `${labelSTR} must be between ${minValue} and ${this._maxValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `${labelSTR} doit être au minimum ${minValue}`,
                            en: `${labelSTR} must be at least ${minValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public max(maxValue: number) {
        /* verifier si le nombre est superieur ou égal à "maxValue" */
        const ruleData = {
            name: 'max',
            init: (value: any) => {
                this._maxValue = maxValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                this._maxValue = maxValue;
    
                valid = (
                    (
                        parseFloat(value) <= maxValue ||
                        (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('min')
                    ) {
                        this.setFinalError({
                            fr: `${labelSTR} doit être comprise entre ${this._minValue} et ${maxValue}`,
                            en: `${labelSTR} must be between ${this._minValue} and ${maxValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `${labelSTR} doit être au maximum ${maxValue}`,
                            en: `${labelSTR} must be maximum ${maxValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public less(lessValue: number) {
        /* verifier si le nombre est inferieur à "lessValue" */
        const ruleData = {
            name: 'less',
            init: (value: any) => {
                this._lessValue = lessValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        parseFloat(value) > lessValue ||
                        (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('greater')
                    ) {
                        this.setFinalError({
                            fr: `${labelSTR} doit être inférieure à ${lessValue} et superieure à ${this._greaterValue}`,
                            en: `${labelSTR} must be less than ${lessValue} and greater than ${this._greaterValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `${labelSTR} doit être inferieure à ${lessValue}`,
                            en: `${labelSTR} must be less than ${lessValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public greater(greaterValue: number) {
        /* verifier si le nombre est superieur à "greaterValue" */
        const ruleData = {
            name: 'greater',
            init: (value: any) => {
                this._greaterValue = greaterValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                this._greaterValue = greaterValue;
    
                valid = (
                    (
                        parseFloat(value) < greaterValue ||
                        (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('less')
                    ) {
                        this.setFinalError({
                            fr: `${labelSTR} doit être inférieure à ${this._lessValue} et superieure à ${greaterValue}`,
                            en: `${labelSTR} must be less than ${this._lessValue} and greater than ${greaterValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `${labelSTR} doit être supérieure à ${greaterValue}`,
                            en: `${labelSTR} must be greater than ${greaterValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public negative() {
        /* verifier si le nombre est negatif */
        const ruleData = {
            name: 'negative',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        parseFloat(value) <= 0 ||
                        (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} doit être un nombre negatif`,
                        en: `${labelSTR} must be a negative number`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public positive() {
        /* verifier si le nombre est positif */
        const ruleData = {
            name: 'positive',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        parseFloat(value) >= 0 ||
                        (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} doit être un nombre positif`,
                        en: `${labelSTR} must be a positive number`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public signed() {
        /* verifier si le nombre est positif ou negatif */
        const ruleData = {
            name: 'signed',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        parseFloat(value) < 0 ||
                        parseFloat(value) > 0
                    ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                ) && this.ruleNames().includes('init');
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} doit être soit un nombre négatif soit un nombre positif`,
                        en: `${labelSTR} must be either a negative number or a positive number`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public decimal(nber: number | undefined = undefined) {
        /* verifier si c'est un nombre decimal. Si nber est renseigné, verifie si le nombre de chiffre après la virgule est egal à "nber" */
        const ruleData = {
            name: 'decimal',
            sanitize: (value: JONRes) => {
                return (value.data !== null) ? parseFloat(`${value.data}`) : null;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                const decimalPart = (
                    parseFloat(String(value))
                ) ? (
                    (
                        String(value).split('.').length === 2
                    ) ? String(value).split('.')[1] : ''
                ) : '';
                if(Config.debug) {
                    // console.log(`> schemas.decimal - JONNumberSchema | decimal - decimalPart:: `, decimalPart);
                }
                valid = (
                    (
                        !!parseFloat(String(value)) &&
                        (
                            typeof decimalPart === 'string' &&
                            ['undefined', 'number'].includes(typeof nber) &&
                            (decimalPart.length > 0 && !nber) ||
                            (decimalPart.length === nber)
                        )
                    ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                ) && this.ruleNames().includes('init');
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} doit être un nombre décimal valide`,
                        en: `${labelSTR} must be a valid decimal number`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public integer() {
        /* verifier si c'est un entier */
        const ruleData = {
            name: 'integer',
            sanitize: (value: JONRes) => {
                return (value.data !== null) ? parseInt(`${value.data}`) : null;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                const decimalPart = (
                    parseFloat(String(value))
                ) ? (
                    (
                        String(value).split('.').length === 2
                    ) ? String(value).split('.')[1] : ''
                ) : '';
                if(Config.debug) {
                    // console.log(`> schemas.integer - JONNumberSchema | integer - decimalPart:: `, decimalPart);
                }
                valid = (
                    (
                        !!parseFloat(String(value)) &&
                        !(
                            typeof decimalPart === 'string' &&
                            decimalPart.length > 0
                        )
                    ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                ) && this.ruleNames().includes('init');
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} doit être un nombre entier valide`,
                        en: `${labelSTR} must be a valid integer number`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public multiple(nber: number) {
        /* verifier si c'est un multiple de "nbre" */
        const ruleData = {
            name: 'multiple',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        !!parseFloat(String(value)) &&
                        parseFloat(String(value)) % nber === 0
                    ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                ) && this.ruleNames().includes('init');
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} doit être un multiple de ${nber}`,
                        en: `${labelSTR} must be a multiple of ${nber}`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public TCPPort() {
        /* verifier si le nombre est un port TCP */
        const ruleData = {
            name: 'tcp-port',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        !!parseFloat(String(value)) &&
                        parseFloat(String(value)) >= 0 &&
                        parseFloat(String(value)) <= 65535
                    ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                ) && this.ruleNames().includes('init');
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} doit être port TCP`,
                        en: `${labelSTR} must be TCP port`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
}
export class JONStringSchema extends JONDefaultSchema<string | number | boolean | null> {
    private _lengthValue?: number = undefined;
    private _maxValue?: number = undefined;
    private _minValue?: number = undefined;
    private _lessValue?: number = undefined;
    private _greaterValue?: number = undefined;
    constructor(lang: 'fr' | 'en' = 'fr') {
        super(lang);

        this.init();
    }

    private init() {
        this._options.validationType = 'string';
        this._options.type = 'string';
        this.initRule();
    }
    private initRule() {
        const cleanValue = (value: any) => {
            if(
                !(
                    value ||
                    typeof(value) === 'boolean' ||
                    (this.getIsUndefined() && !value && typeof value !== 'boolean')
                )
            ) {
                value = this._defaultValue;
            }

            return value;
        }
        // first rule
        const firstRule: JONRule = {
            name: 'init',
            sanitize: (value: JONRes) => {
                if(this.getIsUndefined() && !value.data && typeof value.data != 'boolean') {
                    return undefined
                }
                return (
                    (
                        ['number', 'boolean'].includes(typeof value.data) ||
                        (
                            typeof value.data === 'string' &&
                            value.data.length > 0
                        ) || value.data === null
                    )
                ) ? (
                    (value.data !== null) ? String(value.data) : null
                ) : undefined;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                value = cleanValue(value);
                valid = (
                    (
                        ['number', 'boolean'].includes(typeof value) ||
                        (
                            typeof value === 'string' &&
                            value.length > 0
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    )
                );
                if(Config.debug) {
                    // console.log(`> @hivi.JON.schemas | JONStringSchema - initRule - value:: `, value);
                    // console.log(`> @hivi.JON.schemas | JONStringSchema - initRule - valid:: `, valid);
                }
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}" ` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} est d'un type invalide`,
                        en: `${labelSTR} is of an invalid type`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(firstRule);
    }
    public length(lengthValue: number) {
        /* verifier si la taille de la chaine est égale à "lengthValue" */
        const ruleData = {
            name: 'length',
            init: (value: any) => {
                this._lengthValue = lengthValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            value.length === lengthValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `le nombre de caractères de ${labelSTR} doit être égale ${lengthValue}`,
                        en: `the number of characters of ${labelSTR} must be equal ${lengthValue}`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public min(minValue: number) {
        /* verifier si la taille de la chaine est supérieure ou égal à "minValue" */
        const ruleData = {
            name: 'min',
            init: (value: any) => {
                this._minValue = minValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            value.length >= minValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('max')
                    ) {
                        this.setFinalError({
                            fr: `le nombre de caractères de ${labelSTR} doit être comprise entre ${minValue} et ${this._maxValue}`,
                            en: `the number of characters of ${labelSTR} must be between ${minValue} and ${this._maxValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `le nombre de caractères de ${labelSTR} doit être au minimum ${minValue}`,
                            en: `the number of characters of ${labelSTR} must be at least ${minValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public max(maxValue: number) {
        /* verifier si la taille de la chaine est inférieure ou égal à "maxValue" */
        const ruleData = {
            name: 'max',
            init: (value: any) => {
                this._maxValue = maxValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                this._maxValue = maxValue;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            value.length <= maxValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('min')
                    ) {
                        this.setFinalError({
                            fr: `le nombre de caractères de ${labelSTR} doit être comprise entre ${this._minValue} et ${maxValue}`,
                            en: `the number of characters of ${labelSTR} must be between ${this._minValue} and ${maxValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `le nombre de caractères de ${labelSTR} doit être au maximum ${maxValue}`,
                            en: `the number of characters of ${labelSTR} must be maximum ${maxValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public less(lessValue: number) {
        /* verifier si la taille de la chaine est supérieure à "lessValue" */
        const ruleData = {
            name: 'less',
            init: (value: any) => {
                this._lessValue = lessValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            value.length > lessValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('greater')
                    ) {
                        this.setFinalError({
                            fr: `le nombre de caractères de ${labelSTR} doit être supérieure à ${lessValue} et inférieure à ${this._greaterValue}`,
                            en: `the number of characters of ${labelSTR} must be greater than ${lessValue} and less than ${this._greaterValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `le nombre de caractères de ${labelSTR} doit être supérieure à ${lessValue}`,
                            en: `the number of characters of ${labelSTR} must be greater than ${lessValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public greater(greaterValue: number) {
        /* verifier si la taille de la chaine est inférieure à "greaterValue" */
        const ruleData = {
            name: 'greater',
            init: (value: any) => {
                this._greaterValue = greaterValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                this._greaterValue = greaterValue;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            value.length < greaterValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('less')
                    ) {
                        this.setFinalError({
                            fr: `le nombre de caractères de ${labelSTR} doit être supérieure à ${this._lessValue} et inférieure à ${greaterValue}`,
                            en: `the number of characters of ${labelSTR} must be greater than ${this._lessValue} and less than ${greaterValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `le nombre de caractères de ${labelSTR} doit être inférieure à ${greaterValue}`,
                            en: `the number of characters of ${labelSTR} must be less than ${greaterValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public regexp(ruleValue: RegExp) {
        /* verifier si la chaîne de caractères est respecte la rêgle appliquée */
        const ruleData = {
            name: 'regexp',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            ruleValue.test(value)
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `ne respecte pas la rêgle appliquée`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `does not respect the rule applied`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public alphanum() {
        /* verifier si la chaîne de caractères est alphanumerique */
        const ruleData = {
            name: 'alphanum',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            /^[\w\s]{1,}$/i.test(value)
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne de caractères alphanumeriques`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not a string of alphanumeric characters`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public base64(
        paddingRequired: boolean = true,
        urlSafe: boolean = false,
    ) {
        /* verifier si la chaîne de caractères de type base64 */
        const ruleData = {
            name: 'base64',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                const nbr1 = paddingRequired ? '=': '(={0,1})';
                const nbr2 = urlSafe ? '(\-{1})' : '(+{1})';
                const nbr3 = urlSafe ? '(_{1})' : '(\\{1})';
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            new RegExp(`^(?:[A-Za-z0-9${nbr2}/]{4})*(?:[A-Za-z0-9${nbr2}/]{2}==|[A-Za-z0-9${nbr2}/]{3}${nbr1}|[A-Za-z0-9${nbr2}/]{4})$`).test(value)
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne de caractères de type base64`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not a base64 string`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public lowercase() {
        /* verifier si la chaîne de caractères est en minuscule */
        const ruleData = {
            name: 'lowercase',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            !(/[A-Z]{1,}/.test(value))
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas en minuscule`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not lowercase`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public uppercase() {
        /* verifier si la chaîne de caractères est en majuscule */
        const ruleData = {
            name: 'uppercase',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            !(/[a-z]{1,}/.test(value))
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas en majuscule`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not uppercase`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public toLowercase() {
        /* convertir une chaîne de caractères en minuscule */
        const ruleData = {
            name: 'toLowercase',
            sanitize: (value: JONRes) => {
                if(this.getIsUndefined() && !value.data && typeof value.data != 'boolean') {
                    return undefined
                }
                return (
                    (
                        ['number', 'boolean'].includes(typeof value.data) ||
                        (
                            typeof value.data === 'string' &&
                            value.data.length > 0
                        ) || value.data === null
                    )
                ) ? (
                    (value.data !== null) ? String(value.data).toLowerCase() : null
                ) : undefined;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string'
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `est invalide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is invalid`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public toUppercase() {
        /* convertir une chaîne de caractères en majuscule */
        const ruleData = {
            name: 'toUppercase',
            sanitize: (value: JONRes) => {
                if(this.getIsUndefined() && !value.data && typeof value.data != 'boolean') {
                    return undefined
                }
                return (
                    (
                        ['number', 'boolean'].includes(typeof value.data) ||
                        (
                            typeof value.data === 'string' &&
                            value.data.length > 0
                        ) || value.data === null
                    )
                ) ? (
                    (value.data !== null) ? String(value.data).toUpperCase() : null
                ) : undefined;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string'
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `est invalide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is invalid`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    private CapitalizeAction(value: any):string {
        const sep = ' ';
        if (
            !(
                typeof(value) === 'string'
                && value.length > 0
            )
        ) return value;
        const array_res:string[] = String(value).split(sep).map((val:string) => val.charAt(0).toUpperCase() + val.toLowerCase().slice(1));
        const res:string = array_res.join(sep);
        return res;
    }
    public toCapitalize() {
        /* convertir une chaîne de caractères en lettre capitale */
        const ruleData = {
            name: 'toUppercase',
            sanitize: (value: JONRes) => {
                if(this.getIsUndefined() && !value.data && typeof value.data != 'boolean') {
                    return undefined
                }
                return (
                    (
                        ['number', 'boolean'].includes(typeof value.data) ||
                        (
                            typeof value.data === 'string' &&
                            value.data.length > 0
                        ) || value.data === null
                    )
                ) ? (
                    (value.data !== null) ? this.CapitalizeAction(value.data) : null
                ) : undefined;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string'
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `est invalide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is invalid`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    private UcFirstAction(value: any):string {
        if (
            !(
                typeof(value) === 'string'
                && value.length > 0
            )
        ) return value;
        value = String(value);
        return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
    }
    public toUcFirst() {
        /* convertir la première lettre de la chaine de caractère en majuscule */
        const ruleData = {
            name: 'toUppercase',
            sanitize: (value: JONRes) => {
                if(this.getIsUndefined() && !value.data && typeof value.data != 'boolean') {
                    return undefined
                }
                return (
                    (
                        ['number', 'boolean'].includes(typeof value.data) ||
                        (
                            typeof value.data === 'string' &&
                            value.data.length > 0
                        ) || value.data === null
                    )
                ) ? (
                    (value.data !== null) ? this.UcFirstAction(value.data) : null
                ) : undefined;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string'
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `est invalide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is invalid`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public creditCard(types: ('mastercard' | 'visa' | 'american-express' | 'discover' | 'diners-club' | 'jcb')[] = []) {
        /* verifier si la chaîne de caractères est au format d'un numero de carte de credit */
        types = (
            typeof types === 'object' &&
            Array.isArray(types) === true
        ) ? types : [];
        const ruleData = {
            name: 'creditCard',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                const versionRegEx = {
                    visa: /^(4[0-9]{12}(?:[0-9]{3}))$/i,
                    mastercard: /^((?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12})$/i,
                    'american-express': /^(3[47][0-9]{13})$/i,
                    discover: /^(6(?:011|5[0-9]{2})[0-9]{12})$/i,
                    'diners-club': /^(3(?:0[0-5]|[68][0-9])[0-9]{11})$/i,
                    jcb: /^((?:2131|1800|35\d{3})\d{11})$/i,
                };
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            (
                                (
                                    types.length <= 0 &&
                                    (
                                        versionRegEx.visa.test(value) ||
                                        versionRegEx.mastercard.test(value) ||
                                        versionRegEx.discover.test(value) ||
                                        versionRegEx['american-express'].test(value) ||
                                        versionRegEx['diners-club'].test(value) ||
                                        versionRegEx.jcb.test(value)
                                    )
                                ) || (
                                    types.length > 0 && (
                                        (
                                            types.includes('visa') &&
                                            versionRegEx.visa.test(value)
                                        ) ||
                                        (
                                            types.includes('mastercard') &&
                                            versionRegEx.mastercard.test(value)
                                        ) ||
                                        (
                                            types.includes('discover') &&
                                            versionRegEx.discover.test(value)
                                        ) ||
                                        (
                                            types.includes('american-express') &&
                                            versionRegEx['american-express'].test(value)
                                        ) ||
                                        (
                                            types.includes('diners-club') &&
                                            versionRegEx['diners-club'].test(value)
                                        ) ||
                                        (
                                            types.includes('jcb') &&
                                            versionRegEx.jcb.test(value)
                                        )
                                    )
                                )
                            )
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne de caractères au format d'une carte de crédit`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is a character string in the format of a credit card`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public dataUri() {
        /* verifier si la chaîne de caractères est une chaîne d'URI de données valide */
        const ruleData = {
            name: 'dataUri',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            /^data:([\w\/\+]+);(charset=[\w-]+|base64).*,([a-zA-Z0-9+/]+={0,2})$/.test(value)
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne d'URI de données valide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not a valid data URI string`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public domain() {
        /* verifier si la chaîne de caractères est au format d'un domaine valide */
        const ruleData = {
            name: 'domain',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            new RegExp(`^(?!-)[A-Za-z0-9-]+([\\-\\.]{1}[a-z0-9]+)*\\.[A-Za-z]{2,6}$`).test(value)
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne de caractères est au format d'un domaine valide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not a string is in the format of a valid domain`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public url() {
        /* verifier si la chaîne de caractères est au format d'une url valide */
        const ruleData = {
            name: 'url',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            new RegExp(
                                [
                                    '^(https?:\\/\\/)?',
                                    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|',
                                    '((\\d{1,3}\\.){3}\\d{1,3}))',
                                    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*',
                                    '(\\?[;&a-z\\d%_.~+=-]*)?',
                                    '(\\#[-a-z\\d_]*)?$',
                                ].join(''),
                                'i',
                            ).test(value)
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne de caractères est au format d'une url valide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not a character string is in the format of a valid url`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public hostname() {
        /* verifier si la chaîne de caractères est au format d'un nom d'hôte valide */
        const ruleData = {
            name: 'url',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            (
                                new RegExp(
                                    '^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$',
                                    'i',
                                ).test(value) ||
                                new RegExp(
                                    '^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$',
                                    'i',
                                ).test(value)
                            )
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne de caractères est au format d'un nom d'hôte valide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not a string is in the format of a valid hostname`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public IPAddress(types: ('ipv4' |'ipv6')[] = []) {
        /* verifier si la chaîne de caractères est au format d'une adresse IP valide */
        types = (
            typeof types === 'object' &&
            Array.isArray(types) === true
        ) ? types : [];
        const versionRegEx = {
            ipv4: /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/i,
            ipv6: new RegExp(
                [
                    '(^(?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}$)',
                    '(^::(?:[0-9a-fA-F]{1,4}:){0,6}[0-9a-fA-F]{1,4}$)',
                    '(^[0-9a-fA-F]{1,4}::(?:[0-9a-fA-F]{1,4}:){0,5}[0-9a-fA-F]{1,4}$)',
                    '(^[0-9a-fA-F]{1,4}:[0-9a-fA-F]{1,4}::(?:[0-9a-fA-F]{1,4}:){0,4}[0-9a-fA-F]{1,4}$)',
                    '(^(?:[0-9a-fA-F]{1,4}:){0,2}[0-9a-fA-F]{1,4}::(?:[0-9a-fA-F]{1,4}:){0,3}[0-9a-fA-F]{1,4}$)',
                    '(^(?:[0-9a-fA-F]{1,4}:){0,3}[0-9a-fA-F]{1,4}::(?:[0-9a-fA-F]{1,4}:){0,2}[0-9a-fA-F]{1,4}$)',
                    '(^(?:[0-9a-fA-F]{1,4}:){0,4}[0-9a-fA-F]{1,4}::(?:[0-9a-fA-F]{1,4}:)?[0-9a-fA-F]{1,4}$)',
                    '(^(?:[0-9a-fA-F]{1,4}:){0,5}[0-9a-fA-F]{1,4}::[0-9a-fA-F]{1,4}$)',
                    '(^(?:[0-9a-fA-F]{1,4}:){0,6}[0-9a-fA-F]{1,4}::$)',
                ].join('|'),
                'i',
            ),
        };
        const ruleData = {
            name: 'url',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            (
                                (
                                    types.length <= 0 &&
                                    (
                                        versionRegEx.ipv4.test(value) ||
                                        versionRegEx.ipv6.test(value)
                                    )
                                ) || (
                                    (
                                        types.includes('ipv4') &&
                                        versionRegEx.ipv4.test(value)
                                    ) ||
                                    (
                                        types.includes('ipv6') &&
                                        versionRegEx.ipv6.test(value)
                                    )
                                )
                            )
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne de caractères est au format d'une addresse IP valide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not a character string is in the format of a valid IP address`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public email() {
        /* verifier si la chaîne de caractères est au format d'un email valide */
        const ruleData = {
            name: 'url',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            (
                                /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(value) ||
                                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)
                            )
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne de caractères est au format d'un email valide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not a character string is in the format of a valid email`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public guid(versions: ('v1' | 'v2' | 'v3' | 'v4' | 'v5')[] = ['v1', 'v2', 'v3', 'v4', 'v5']) {
        /* verifier si la chaîne de caractères est au format d'un GUID */
        const versionRegEx = {
            v1: /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[1][0-9a-fA-F]{3}-[89AB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}$/i,
            v2: /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[2][0-9a-fA-F]{3}-[89AB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}$/i,
            v3: /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[3][0-9a-fA-F]{3}-[89AB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}$/i,
            v4: /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[4][0-9a-fA-F]{3}-[89AB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}$/i,
            v5: /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[5][0-9a-fA-F]{3}-[89AB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}$/i,
        };
        const ruleData = {
            name: 'url',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            (
                                (
                                    versions.length <= 0 && (
                                        versionRegEx.v1.test(value) ||
                                        versionRegEx.v2.test(value) ||
                                        versionRegEx.v3.test(value) ||
                                        versionRegEx.v4.test(value) ||
                                        versionRegEx.v5.test(value)
                                    )
                                ) || (
                                    (
                                        !!versions.includes('v1') &&
                                        versionRegEx.v1.test(value)
                                    ) ||
                                    (
                                        !!versions.includes('v2') &&
                                        versionRegEx.v2.test(value)
                                    ) ||
                                    (
                                        !!versions.includes('v3') &&
                                        versionRegEx.v3.test(value)
                                    ) ||
                                    (
                                        !!versions.includes('v4') &&
                                        versionRegEx.v4.test(value)
                                    ) ||
                                    (
                                        !!versions.includes('v5') &&
                                        versionRegEx.v5.test(value)
                                    )
                                )
                            )
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne de caractères est au format GUID valide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not a character string is in valid GUID format`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public hexa(insensitive: boolean = false) {
        /* verifier si la chaîne de caractères est au format hexadécimal */
        const ruleData = {
            name: 'url',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            (
                                (
                                    insensitive &&
                                    /^[0-9a-fA-F]+$/i.test(value)
                                ) ||
                                (
                                    !insensitive &&
                                    /^[0-9A-F]+$/i.test(value)
                                )
                            )
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne de caractères est au format hexadécimal valide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not a string is in valid hexadecimal format`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public binary() {
        /* verifier si la chaîne de caractères est au format binaire */
        const ruleData = {
            name: 'binary',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            /^[0-1]{1,}$/i.test(value)
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne de caractères est au format binaire valide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not a string is in valid binary format`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public date(
        format: string | undefined = undefined,
    ) {
        /* verifier si la chaîne de caractères est au format de type date */
        const ruleData = {
            name: 'date',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            new Timez(value, format).isValid()
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne de caractères est au format d'une date valide`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not a character string is in the format of a valid date`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public identifier() {
        /* verifier si la chaîne de caractères est un idientifiant */
        const ruleData = {
            name: 'identifier',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            typeof(value) === 'string' &&
                            /^[a-zA-Z]{1,}\w{0,}$/i.test(value)
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: [
                            labelSTR,
                            `n'est pas une chaîne de caractères sous le format d'un identifiant`,
                        ].join(' '),
                        en: [
                            labelSTR,
                            `is not a character string in the format of an identifier`,
                        ].join(' '),
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
}
export class JONBooleanSchema extends JONDefaultSchema<any> {
    private _trueValues: any[] = ['true', 't', '1', 1, true];
    private _falseValues: any[] = ['false', 'f', '0', 0, false];

    constructor(lang: 'fr' | 'en' = 'fr') {
        super(lang);

        this.init();
    }

    private init() {
        this._options.validationType = 'boolean';
        this._options.type = 'boolean';
        this.initRule();
    }
    private initRule() {
        const cleanValue = (value: any) => {
            if(
                !(
                    typeof value === 'boolean' ||
                    (this._trueValues.concat(this._falseValues).map(mapFunct_cleanString)).includes(value) ||
                    (this.getIsUndefined() && !value && typeof value !== 'boolean')
                )
            ) {
                value = this._defaultValue;
            }

            return value;
        };
        // first rule
        const mapFunct_cleanString = (val: any) => ((typeof val === 'string') ? val.toLowerCase() : val);
        const firstRule: JONRule = {
            name: 'init',
            sanitize: (value: JONRes) => {
                if(this.getIsUndefined() && !value.data && typeof value.data != 'boolean') {
                    return undefined
                }
                value.data = mapFunct_cleanString(value.data);
                return (
                    (
                        (
                            typeof value.data === 'boolean' ||
                            (this._trueValues.concat(this._falseValues).map(mapFunct_cleanString)).includes(value.data)
                        ) || value.data === null
                    )
                ) ? (
                    (value.data !== null) ? (
                        (this._trueValues.map(mapFunct_cleanString).includes(value.data)) ? true : false
                    ) : null
                ) : undefined;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

    
                value = mapFunct_cleanString(value);
                value = cleanValue(value);
                valid = (
                    (
                        (
                            typeof value === 'boolean' ||
                            (this._trueValues.concat(this._falseValues).map(mapFunct_cleanString)).includes(value)
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    )
                );
                if(Config.debug) {
                    // console.log(`> @hivi.JON.schemas | JONBooleanSchema - initRule - this._trueValues.concat(this._falseValues).map(mapFunct_cleanString):: `, this._trueValues.concat(this._falseValues).map(mapFunct_cleanString));
                    // console.log(`> @hivi.JON.schemas | JONBooleanSchema - initRule - (this._trueValues.concat(this._falseValues).map(mapFunct_cleanString)).includes(value):: `, (this._trueValues.concat(this._falseValues).map(mapFunct_cleanString)).includes(value));
                    console.log(`> @hivi.JON.schemas | JONBooleanSchema - initRule - value:: `, value);
                    console.log(`> @hivi.JON.schemas | JONBooleanSchema - initRule - valid:: `, valid);
                }
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}" ` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} est d'un type invalide`,
                        en: `${labelSTR} is of an invalid type`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(firstRule);
    }

    public trueValues(values: any[]) {
        values = (
            typeof values === 'object' &&
            Array.isArray(values) === true
        ) ? values : [];
        this._trueValues = values;

        return this;
    }
    public falseValues(values: any[]) {
        values = (
            typeof values === 'object' &&
            Array.isArray(values) === true
        ) ? values : [];
        this._falseValues = values;

        return this;
    }

    public default(defaultValue: any) {
        this._defaultValue = (
            this._trueValues.map(
                (val) => String(val).toLowerCase()
            ).includes(
                String(defaultValue).toLowerCase()
            )
        ) ? true : false;
        return this;
    }
}
export class JONDateSchema extends JONDefaultSchema<string | number | Date | null> {
    private _maxValue?: Date = undefined;
    private _minValue?: Date = undefined;
    private _lessValue?: Date = undefined;
    private _greaterValue?: Date = undefined;
    private _defaultFormat: string = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]';// 'YYYY-MM-DD HH:mm:ss.SSS [GMT]ZZ';
    private _format: string | undefined = this._defaultFormat;
    constructor(lang: 'fr' | 'en' = 'fr') {
        super(lang);

        this.init();
    }

    private init() {
        this._options.validationType = 'date';
        this._options.type = 'date';
        this.initRule();
    }
    private initRule() {
        const cleanValue = (value: any) => {
            if(
                !(
                    value ||
                    this.checkIsUndefined(value)
                )
            ) {
                value = this._defaultValue;
            }

            return value;
        };
        // first rule
        const firstRule: JONRule = {
            name: 'init',
            sanitize: (value: JONRes) => {
                if(this.checkIsUndefined(value.data)) {
                    return undefined
                }
                return (
                    (
                        new Timez(value.data, this._format, this._lang).isValid() ||
                        value.data === null
                    )
                ) ? (
                    (value.data !== null) ? new Timez(value.data, this._format, this._lang).toDate() : null
                ) : undefined;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                value = cleanValue(value);
                valid = (
                    (
                        new Timez(value, this._format, this._lang).isValid() ||
                        (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    )
                );
                if(Config.debug) {
                    // console.log(`> @hivi.JON.schemas | JONDateSchema - initRule - value:: `, value);
                    // console.log(`> @hivi.JON.schemas | JONDateSchema - initRule - valid:: `, valid);
                }
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}" ` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} est une date`,
                        en: `${labelSTR} is a date`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(firstRule);
    }
    public min(minValue: any) {
        /* verifier si la date est supérieure ou égale à "minValue" */
        const anyIsDate = (value: any) => (
            new Timez(value, this._format, this._lang).isValid() 
        );
        const anyToDate = (value: any) => ((anyIsDate(value)) ? new Timez(value, this._format, this._lang).toDate() : undefined);
        const anyToTimeDate = (value: any) => ((anyIsDate(value)) ? new Timez(value, this._format, this._lang).toDate()?.getTime() : 0);
        const ruleData = {
            name: 'min',
            init: (value: any) => {
                this._minValue = anyToDate(minValue);
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                const anyToTimeDateValue = anyToTimeDate(value);
    
                valid = (
                    (
                        (
                            !!anyIsDate(value) &&
                            !!this._minValue &&
                            !!anyToTimeDateValue &&
                            anyToTimeDateValue >= this._minValue.getTime()
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'la date',
                        en: 'the date'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('max')
                    ) {
                        this.setFinalError({
                            fr: `${labelSTR} doit être comprise entre ${anyToDate(minValue)} et ${this._maxValue}`,
                            en: `${labelSTR} must be between ${anyToDate(minValue)} and ${this._maxValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `${labelSTR} doit être supérieure ou égale ${anyToDate(minValue)}`,
                            en: `${labelSTR} must be less than or equal ${anyToDate(minValue)}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public max(maxValue: any) {
        /* verifier si la date est inférieure ou égal à "maxValue" */
        const anyIsDate = (value: any) => (
            new Timez(value, this._format, this._lang).isValid() 
        );
        const anyToDate = (value: any) => ((anyIsDate(value)) ? new Timez(value, this._format, this._lang).toDate() : undefined);
        const anyToTimeDate = (value: any) => ((anyIsDate(value)) ? new Timez(value, this._format, this._lang).toDate()?.getTime() : 0);
        const ruleData = {
            name: 'max',
            init: (value: any) => {
                this._maxValue = anyToDate(maxValue);
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                const anyToTimeDateValue = anyToTimeDate(value);
    
                valid = (
                    (
                        (
                            !!anyIsDate(value) &&
                            !!this._maxValue &&
                            !!anyToTimeDateValue &&
                            anyToTimeDateValue <= this._maxValue.getTime()
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'la date',
                        en: 'the date'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('min')
                    ) {
                        this.setFinalError({
                            fr: `${labelSTR} doit être comprise entre ${this._minValue} et ${anyToDate(maxValue)}`,
                            en: `${labelSTR} must be between ${this._minValue} and ${anyToDate(maxValue)}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `${labelSTR} doit être inférieure ou égale ${anyToDate(maxValue)}`,
                            en: `${labelSTR} must be less than or equal ${anyToDate(maxValue)}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public less(lessValue: any) {
        /* verifier si la date doit être supérieure à "lessValue" */
        const anyIsDate = (value: any) => (
            new Timez(value, this._format, this._lang).isValid() 
        );
        const anyToDate = (value: any) => ((anyIsDate(value)) ? new Timez(value, this._format, this._lang).toDate() : undefined);
        const anyToTimeDate = (value: any) => ((anyIsDate(value)) ? new Timez(value, this._format, this._lang).toDate()?.getTime() : 0);
        const ruleData = {
            name: 'less',
            init: (value: any) => {
                this._lessValue = anyToDate(lessValue);
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                const anyToTimeDateValue = anyToTimeDate(value);
    
                valid = (
                    (
                        (
                            !!anyIsDate(value) &&
                            !!this._lessValue &&
                            !!anyToTimeDateValue &&
                            anyToTimeDateValue > this._lessValue.getTime()
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'la date',
                        en: 'the date',
                    }[this._lang];
                    if(
                        this.ruleNames().includes('greater')
                    ) {
                        this.setFinalError({
                            fr: `${labelSTR} doit être inférieur à ${this._greaterValue} et superieur à ${anyToDate(lessValue)}`,
                            en: `${labelSTR} must be less than ${this._greaterValue} and greater than ${anyToDate(lessValue)}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `${labelSTR} doit être supérieure à ${anyToDate(lessValue)}`,
                            en: `${labelSTR} must be greater than ${anyToDate(lessValue)}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public greater(greaterValue: any) {
        /* verifier si la date est inférieure à "greaterValue" */
        const anyIsDate = (value: any) => (
            new Timez(value, this._format, this._lang).isValid() 
        );
        const anyToDate = (value: any) => ((anyIsDate(value)) ? new Timez(value, this._format, this._lang).toDate() : undefined);
        const anyToTimeDate = (value: any) => ((anyIsDate(value)) ? new Timez(value, this._format, this._lang).toDate()?.getTime() : 0);
        const ruleData = {
            name: 'greater',
            init: (value: any) => {
                this._greaterValue = anyToDate(greaterValue);
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                const anyToTimeDateValue = anyToTimeDate(value);
    
                valid = (
                    (
                        (
                            !!anyIsDate(value) &&
                            !!this._greaterValue &&
                            !!anyToTimeDateValue &&
                            anyToTimeDateValue < this._greaterValue.getTime()
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'la date',
                        en: 'the date'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('less')
                    ) {
                        this.setFinalError({
                            fr: `${labelSTR} doit être inférieur à ${anyToDate(greaterValue)} et superieure à ${this._lessValue}`,
                            en: `${labelSTR} must be less than ${anyToDate(greaterValue)} and greater than ${this._lessValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `${labelSTR} doit être inférieure à ${anyToDate(greaterValue)}`,
                            en: `${labelSTR} must be less than ${anyToDate(greaterValue)}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }

    public format(format: string | undefined, strict: boolean = false) {
        this._format = (!!format) ? format : ((!!strict) ? undefined : this._defaultFormat);
        return this;
    }
    public default(defaultValue: string | number | Date | null) {
        const anyIsDate = (value: any) => (
            new Timez(value, this._format, this._lang).isValid() 
        );
        const anyToDate = (value: any) => ((anyIsDate(value)) ? new Timez(value, this._format, this._lang).toDate() : undefined);
        
        if(anyIsDate(defaultValue)) {
            this._defaultValue = anyToDate(defaultValue);
        }
        if(defaultValue === null) {
            this._defaultValue = null;
        }
        return this;
    }
}
export class JONEnumSchema extends JONDefaultSchema<any> {
    private _choices: any[] = [];
    constructor(lang: 'fr' | 'en' = 'fr') {
        super(lang);

        this.init();
    }

    private init() {
        this._options.validationType = 'enum';
        this._options.type = 'enum';
        this.initRule();
    }
    private initRule() {
        // first rule
        const mapFunct_clean = (val: any) => {
            let res = val;
            if(typeof val === 'string') {
                res = val.toLowerCase();
            }
            if(parseFloat(res)) {
                res = parseFloat(res);
            } else if(['true', 't', 1, true, 'false', 'f', 0, false].includes(res)) {
                res = (['true', 't', 1, true].includes(res)) ? true : false;
            } else if(new Timez(res, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]', this._lang).isValid()) {
                res = new Timez(res, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]', this._lang).toDate();
            } else if(this.checkIsUndefined(val)) {
                res = undefined;
            }

            return res;
        };
        const enumValueIsValid = (value: any) => {
            this._choices = this._choices.map(mapFunct_clean);
            // console.log(`> @hivi.JON.schemas | JONEnumSchema - initRule - enumValueIsValid - this._choices:: `, this._choices);

            if( this.checkIsUndefined(value) ) {
                return true;
            } else {
                let res = (
                    (
                        typeof this._choices === 'object' &&
                        Array.isArray(this._choices) === true &&
                        (
                            (
                                this._choices.length > 0 &&
                                (
                                    this._choices.includes(value)
                                )
                            ) || this._choices.length <= 0
                        )
                    ) ||
                    (this.getIsUndefined() && !value && typeof value !== 'boolean')
                );
    
                return res;
            }
        };
        const cleanValue = (value: any) => {
            if(
                !(
                    value ||
                    typeof(value) === 'boolean' ||
                    this.checkIsUndefined(value)
                )
            ) {
                value = this._defaultValue;
            }

            return value;
        };
        const firstRule: JONRule = {
            name: 'init',
            sanitize: (value: JONRes) => {
                if(this.checkIsUndefined(value)) {
                    return undefined
                }
                // console.log(`> @hivi.JON.schemas | JONEnumSchema - sanitize - value.data:: `, value.data);
                return (enumValueIsValid(value.data)) ? (
                    (value.data !== null) ? value.data : null
                ) : undefined;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                value = cleanValue(value);
                valid = (enumValueIsValid(value));

                if(Config.debug) {
                    // console.log(`> @hivi.JON.schemas | JONEnumSchema - initRule - value:: `, value);
                    // console.log(`> @hivi.JON.schemas | JONEnumSchema - initRule - valid:: `, valid);
                }
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}" ` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} est invalide`,
                        en: `${labelSTR} is invalid`,
                    }[this._lang]);
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(firstRule);
    }

    public choices(...values: any[]) {
        this._choices = (
            typeof values === 'object' &&
            Array.isArray(values) === true &&
            values.length > 0
        ) ? values : [];

        return this;
    }
    public getChoices() {
        return this._choices;
    }
}
export class JONNotInEnumSchema extends JONDefaultSchema<any> {
    private _choices: any[] = [];
    constructor(lang: 'fr' | 'en' = 'fr') {
        super(lang);

        this.init();
    }

    private init() {
        this._options.validationType = 'enum';
        this._options.type = 'enum';
        this.initRule();
    }
    private initRule() {
        // first rule
        const mapFunct_clean = (val: any) => {
            let res = val;
            if(typeof val === 'string') {
                res = val.toLowerCase();
            }
            if(parseFloat(res)) {
                res = parseFloat(res);
            } else if(['true', 't', 1, true, 'false', 'f', 0, false].includes(res)) {
                res = (['true', 't', 1, true].includes(res)) ? true : false;
            } else if(new Timez(res, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]', this._lang).isValid()) {
                res = new Timez(res, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]', this._lang).toDate();
            } else if(this.checkIsUndefined(val)) {
                res = undefined;
            }

            return res;
        };
        const enumValueIsValid = (value: any) => {
            this._choices = this._choices.map(mapFunct_clean);

            if( this.checkIsUndefined(value) ) {
                return true;
            } else {
                let res = (
                    (
                        typeof this._choices === 'object' &&
                        Array.isArray(this._choices) === true &&
                        (
                            (
                                this._choices.length > 0 &&
                                (
                                    !this._choices.includes(value)
                                )
                            ) || this._choices.length <= 0
                        )
                    ) ||
                    (this.getIsUndefined() && !value && typeof value !== 'boolean')
                );
    
                return res;
            }
        };
        const cleanValue = (value: any) => {
            if(
                !(
                    value ||
                    typeof(value) === 'boolean' ||
                    this.checkIsUndefined(value)
                )
            ) {
                value = this._defaultValue;
            }

            return value;
        };
        const firstRule: JONRule = {
            name: 'init',
            sanitize: (value: JONRes) => {
                if(this.checkIsUndefined(value)) {
                    return undefined
                }
                // console.log(`> @hivi.JON.schemas | JONNotInEnumSchema - sanitize - value.data:: `, value.data);
                return (enumValueIsValid(value.data)) ? (
                    (value.data !== null) ? value.data : null
                ) : undefined;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                value = cleanValue(value);
                valid = (enumValueIsValid(value));

                if(Config.debug) {
                    // console.log(`> @hivi.JON.schemas | JONNotInEnumSchema - initRule - value:: `, value);
                    // console.log(`> @hivi.JON.schemas | JONNotInEnumSchema - initRule - valid:: `, valid);
                }
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}" ` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} est invalide`,
                        en: `${labelSTR} is invalid`,
                    }[this._lang]);
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(firstRule);
    }

    public choices(...values: any[]) {
        this._choices = (
            typeof values === 'object' &&
            Array.isArray(values) === true &&
            values.length > 0
        ) ? values : [];

        return this;
    }
    public getChoices() {
        return this._choices;
    }
}
export class JONChosenTypeSchema extends JONDefaultSchema<any> {
    private _choices = [].map((value: never, index: number) => convertInCorrectSchemaType(value));
    constructor(lang: 'fr' | 'en' = 'fr') {
        super(lang);

        this.init();
    }

    private init() {
        this._options.validationType = 'any';
        this._options.type = 'any';
        this.initRule();
    }
    private initRule() {
        // first rule
        const mapFunct_clean = (val: any) => {
            let res = val;
            if(typeof val === 'string') {
                res = val.toLowerCase();
            }
            // console.log(`--------> @hivi.JON.schemas | JONChosenTypeSchema - enumValueIsValid - new JONDateSchema(this._lang).required().isValid(res):: `, new JONDateSchema(this._lang).required().isValid(res));
            if(parseFloat(res)) {
                res = parseFloat(res);
            } else if(['true', 't', 1, true, 'false', 'f', 0, false].includes(res)) {
                res = (['true', 't', 1, true].includes(res)) ? true : false;
            } else if( new JONDateSchema(this._lang).required().isValid(res)) {
                res = new Timez(res, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]', this._lang).toDate();
            } else if(this.checkIsUndefined(val)) {
                res = undefined;
            }

            return res;
        };
        const enumValueIsValid = (value: any) => {
            value = mapFunct_clean(value);
            // console.log(`--------> @hivi.JON.schemas | JONChosenTypeSchema - enumValueIsValid - value:: `, value);
            // console.log(`--------> @hivi.JON.schemas | JONChosenTypeSchema - enumValueIsValid - this.checkIsUndefined(value):: `, this.checkIsUndefined(value));
            if(this.checkIsUndefined(value)) {
                return true;
            } else {
                let res = (
                    (
                        this._choices.filter((choice) => !!choice.isValid(value)).length > 0
                    ) ||
                    this.checkIsUndefined(value)
                );
                return res;
            }
        };
        const cleanValue = (value: any) => {
            if(
                !(
                    value ||
                    this.checkIsUndefined(value)
                )
            ) {
                value = this._defaultValue;
            }

            return value;
        };
        const firstRule: JONRule = {
            name: 'init',
            sanitize: (value: JONRes) => {
                if(this.getIsUndefined() && !value.data && typeof value.data != 'boolean') {
                    return undefined
                }
                return (enumValueIsValid(value.data)) ? (
                    (value.data !== null) ? value.data : null
                ) : undefined;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                value = cleanValue(value);
                valid = (enumValueIsValid(value));

                if(Config.debug) {
                    // console.log(`> @hivi.JON.schemas | JONChosenTypeSchema - initRule - value:: `, value);
                    // console.log(`> @hivi.JON.schemas | JONChosenTypeSchema - initRule - valid:: `, valid);
                    // console.log(`> @hivi.JON.schemas | JONChosenTypeSchema - initRule - this.checkIsUndefined(value):: `, this.checkIsUndefined(value));
                }
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}" ` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} est invalide`,
                        en: `${labelSTR} is invalid`,
                    }[this._lang]);
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(firstRule);
    }

    public choices(...values: any[]) {
        this._choices = (
            values.filter((val) => checkIfCorrectTypeSchema(val)).length > 0
        ) ? values : [];

        return this;
    }
    public getChoices() {
        return this._choices;
    }
}
export class JONFileSchema extends JONDefaultSchema<any> {
    private _lengthValue?: number = undefined;
    private _maxValue?: number = undefined;
    private _minValue?: number = undefined;
    private _lessValue?: number = undefined;
    private _greaterValue?: number = undefined;
    private _filename?: string | RegExp = undefined;
    private _defaultFileType: any | undefined = undefined;
    public regexExpressionsPossibles = [
        /^(audio){1}\/\w{1,}$/gmi,
        /^(audio){1}\/(webm){1}$/gmi,
        /^(audio){1}\/(3gpp){1}$/gmi,
        /^(audio){1}\/(3gpp2){1}$/gmi,

        /^(video){1}\/\w{1,}$/gmi,
        /^(video){1}\/(mpeg){1}$/gmi,
        /^(video){1}\/(webm){1}$/gmi,
        /^(video){1}\/(3gpp){1}$/gmi,
        /^(video){1}\/(3gpp2){1}$/gmi,

        /^(image){1}\/\w{1,}$/gmi,
        /^(image){1}\/(svg\+xml){1}$/gmi,
        /^(image){1}\/(png){1}$/gmi,
        /^(image){1}\/(jpeg){1}$/gmi,

        /^(document){1}\/\w{1,}$/gmi,
        /^(document){1}\/(pdf){1}$/gmi,

        /^(application){1}\/\w{1,}$/gmi,
        /^(application){1}\/(vnd.ms-excel){1}$/gmi,
        /^(application){1}\/(vnd.openxmlformats-officedocument.spreadsheetml.sheet){1}$/gmi,
        /^(application){1}\/(xml){1}$/gmi,
        /^(application){1}\/(zip){1}$/gmi,
        /^(application){1}\/(rar){1}$/gmi,

        /^(font){1}\/\w{1,}$/gmi,
        /^(font){1}\/(woff){1}$/gmi,
        /^(font){1}\/(woff2){1}$/gmi,
        /^(font){1}\/(otf){1}$/gmi,
    ];
    constructor(lang: 'fr' | 'en' = 'fr') {
        super(lang);

        this.init();
    }

    private init() {
        this._options.validationType = 'file';
        this._options.type = 'file';
        this.initRule();
    }
    private fileValueIsValidPreSUB(value: any) {
        const defaultFileType = this._defaultFileType;
        return (
            isBlob(value) ||
            isFile(value) ||
            // value instanceof File ||
            // value instanceof Blob ||
            (
                this._defaultFileType &&
                value instanceof defaultFileType
            )
        );
    }
    private fileValueIsValidSUB(value: any) {
        return (
            this.fileValueIsValidPreSUB(value)
        );
    }
    private fileValueIsValid(value: any) {
        return (
            this.fileValueIsValidSUB(value) ||
            (this.getIsUndefined() && !value && typeof value !== 'boolean')
        );
    }
    private initRule() {
        // first rule
        const firstRule: JONRule = {
            name: 'init',
            sanitize: (value: JONRes) => {
                if(this.getIsUndefined() && !value.data && typeof value.data != 'boolean') {
                    return undefined
                }
                // console.log(`> @hivi.JON.schemas | JONFileSchema - sanitize - value.data:: `, value.data);
                return (this.fileValueIsValid(value.data)) ? (
                    (value.data !== null) ? value.data : null
                ) : undefined;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = this.fileValueIsValid(value);

                if(Config.debug) {
                    // console.log(`> @hivi.JON.schemas | JONFileSchema - initRule - value:: `, value);
                    // console.log(`> @hivi.JON.schemas | JONFileSchema - initRule - valid:: `, valid);
                }
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}" ` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} est invalide`,
                        en: `${labelSTR} is invalid`,
                    }[this._lang]);
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(firstRule);
    }

    public filename(filename: string | RegExp) {
        /* verifier si le nom de fichier a la valeur requise */
        const ruleData = {
            name: 'filename',
            init: (value: any) => {
                this._filename = filename;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            this.fileValueIsValidPreSUB(value) &&
                            (
                                (
                                    typeof filename === 'string' &&
                                    filename.length > 0 &&
                                    (
                                        (
                                            typeof value.name === 'string' &&
                                            value.name.length > 0 &&
                                            value.name === filename
                                        ) ||
                                        (
                                            typeof value.newFilename === 'string' &&
                                            value.newFilename.length > 0 &&
                                            value.newFilename === filename
                                        ) ||
                                        (
                                            typeof value.originalFilename === 'string' &&
                                            value.originalFilename.length > 0 &&
                                            value.originalFilename === filename
                                        )
                                    )
                                ) || (
                                    filename instanceof RegExp &&
                                    (
                                        (
                                            typeof value.name === 'string' &&
                                            value.name.length > 0 &&
                                            filename.test(value.name)
                                        ) ||
                                        (
                                            typeof value.newFilename === 'string' &&
                                            value.newFilename.length > 0 &&
                                            filename.test(value.newFilename)
                                        ) ||
                                        (
                                            typeof value.originalFilename === 'string' &&
                                            value.originalFilename.length > 0 &&
                                            filename.test(value.originalFilename)
                                        )
                                    )
                                )
                            )
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} n'a pas la valeur requise`,
                        en: `${labelSTR} does not have the required value`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public type(...types: (
        string |
        RegExp
    )[]) {
        types =(
            typeof types === 'object' &&
            Array.isArray(types) === true
        ) ? types : [];
        /* verifier si la taille du fichier est égale à "lengthValue" */
        const ruleData = {
            name: 'type',
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            this.fileValueIsValidPreSUB(value) &&
                            types.filter((type) => (
                                (
                                    typeof type === 'string' &&
                                    type.length > 0 &&
                                    (
                                        (
                                            typeof value.type === 'string' &&
                                            value.type.length > 0 &&
                                            value.type === type
                                        ) ||
                                        (
                                            typeof value.mimetype === 'string' &&
                                            value.mimetype.length > 0 &&
                                            value.mimetype === type
                                        )
                                    )
                                ) || (
                                    type instanceof RegExp &&
                                    (
                                        (
                                            typeof value.type === 'string' &&
                                            value.type.length > 0 &&
                                            type.test(value.type)
                                        ) ||
                                        (
                                            typeof value.mimetype === 'string' &&
                                            value.mimetype.length > 0 &&
                                            type.test(value.mimetype)
                                        )
                                    )
                                )
                            )).length > 0
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `${labelSTR} n'est pas d'un type autorisé`,
                        en: `${labelSTR} is not of an authorized type`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public length(lengthValue: number) {
        /* verifier si la taille du fichier est égale à "lengthValue" */
        const ruleData = {
            name: 'length',
            init: (value: any) => {
                this._lengthValue = lengthValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            this.fileValueIsValidPreSUB(value) &&
                            value.size === lengthValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    this.setFinalError({
                        fr: `la taille de ${labelSTR} doit être égale ${lengthValue}`,
                        en: `the size of ${labelSTR} must be equal ${lengthValue}`,
                    });
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public min(minValue: number) {
        /* verifier si la taille du fichier est supérieure ou égal à "minValue" */
        const ruleData = {
            name: 'min',
            init: (value: any) => {
                this._minValue = minValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            this.fileValueIsValidPreSUB(value) &&
                            value.size >= minValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('max')
                    ) {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être comprise entre ${minValue} et ${this._maxValue}`,
                            en: `the size of ${labelSTR} must be between ${minValue} and ${this._maxValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être au minimum ${minValue}`,
                            en: `the size of ${labelSTR} must be at least ${minValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public max(maxValue: number) {
        /* verifier si la taille du fichier est inférieure ou égal à "maxValue" */
        const ruleData = {
            name: 'max',
            init: (value: any) => {
                this._maxValue = maxValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                this._maxValue = maxValue;
    
                valid = (
                    (
                        (
                            this.fileValueIsValidPreSUB(value) &&
                            value.size <= maxValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('min')
                    ) {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être comprise entre ${this._minValue} et ${maxValue}`,
                            en: `the size of ${labelSTR} must be between ${this._minValue} and ${maxValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être au maximum ${maxValue}`,
                            en: `the size of ${labelSTR} must be maximum ${maxValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public less(lessValue: number) {
        /* verifier si la taille du fichier est supérieure à "lessValue" */
        const ruleData = {
            name: 'less',
            init: (value: any) => {
                this._lessValue = lessValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;
    
                valid = (
                    (
                        (
                            this.fileValueIsValidPreSUB(value) &&
                            value.size > lessValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('greater')
                    ) {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être supérieure à ${lessValue} et inférieure à ${this._greaterValue}`,
                            en: `the size of ${labelSTR} must be greater than ${lessValue} and less than ${this._greaterValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être supérieure à ${lessValue}`,
                            en: `the size of ${labelSTR} must be greater than ${lessValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }
    public greater(greaterValue: number) {
        /* verifier si la taille du fichier est inférieure à "greaterValue" */
        const ruleData = {
            name: 'greater',
            init: (value: any) => {
                this._greaterValue = greaterValue;
                return value;
            },
            rule: (value: any) => {
                let data: any | undefined;
                let valid: boolean = false;
                let error: Error | undefined;

                this._greaterValue = greaterValue;
    
                valid = (
                    (
                        (
                            this.fileValueIsValidPreSUB(value) &&
                            value.size < greaterValue
                        ) || (this.getIsUndefined() && !value && typeof value !== 'boolean')
                    ) &&
                    this.ruleNames().includes('init')
                );
                if(valid) {
                    error = undefined;
            
                    data = value;
                } else {
                    const labelSTR = this.getLabel() ? `"${this.getLabel()}"` : {
                        fr: 'l\'element',
                        en: 'the element'
                    }[this._lang];
                    if(
                        this.ruleNames().includes('less')
                    ) {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être supérieure à ${this._lessValue} et inférieure à ${greaterValue}`,
                            en: `the size of ${labelSTR} must be greater than ${this._lessValue} and less than ${greaterValue}`,
                        });
                    } else {
                        this.setFinalError({
                            fr: `la taille de ${labelSTR} doit être inférieure à ${greaterValue}`,
                            en: `the size of ${labelSTR} must be less than ${greaterValue}`,
                        });
                    }
                    error = this.getError();
                    data = undefined;
                }
        
                return {
                    data,
                    valid,
                    error,
                };
            },
        };
        this._options.rules.push(ruleData);

        return this;
    }

    public changeFileType(type: any) {
        try {
            this._defaultFileType = type;
        } catch (error) {
            this._defaultFileType = undefined;
            if(this._defaultFileType instanceof type) {}
        }

        return this;
    }
}


export class JONClone {
    private _target: string | undefined;
    constructor(target: string, lang: 'fr' | 'en' = 'fr') {
        // super(lang);

        // this.init();

        this.target = target;
    }

    public get target() {
        return this._target;
    }
    public set target(target: string | undefined) {
        this._target = (
            typeof target === 'string' &&
            target.length > 0
        ) ? target : undefined;
    }
}
export class JONRef {
    private _target: string | undefined;
    constructor(target: string, lang: 'fr' | 'en' = 'fr') {
        // super(lang);

        // this.init();

        this.target = target;
    }

    public get target() {
        return this._target;
    }
    public set target(target: string | undefined) {
        this._target = (
            typeof target === 'string' &&
            target.length > 0
        ) ? target : undefined;
    }
}

const checkIfCorrectTypeSchema = (value: any) => (
    value instanceof JONNumberSchema ||
    value instanceof JONStringSchema ||
    value instanceof JONBooleanSchema ||
    value instanceof JONDateSchema ||
    value instanceof JONEnumSchema ||
    value instanceof JONNotInEnumSchema ||
    value instanceof JONObjectSchema ||
    value instanceof JONArraySchema ||
    value instanceof JONChosenTypeSchema ||
    value instanceof JONAnySchema ||
    value instanceof JONFileSchema
);
const convertInCorrectSchemaType = (value: any) => (
    value as (
        JONNumberSchema |
        JONStringSchema |
        JONBooleanSchema |
        JONDateSchema |
        JONEnumSchema |
        JONNotInEnumSchema |
        JONObjectSchema |
        JONArraySchema |
        JONChosenTypeSchema |
        JONAnySchema |
        JONFileSchema
    )
)