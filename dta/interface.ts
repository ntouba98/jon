
export interface JONRes {
    data?: any;
    valid: boolean;
    error?: Error;
};
export interface JONRule {
    name: string;
    init?: (value: any) => void;
    rule?: (value: any) => JONRes;
    sanitize?: (value: JONRes) => any;
};