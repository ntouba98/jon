import {
    JONArraySchema,
    JONBooleanSchema,
    JONChosenTypeSchema,
    JONDateSchema,
    JONEnumSchema,
    JONNotInEnumSchema,
    JONNumberSchema,
    JONObjectSchema,
    JONStringSchema,
    JONClone,
    JONRef,
    JONAnySchema,
    JONFileSchema,
} from './dta/schemas'


export const Number = JONNumberSchema;
export const Boolean = JONBooleanSchema;
export const String = JONStringSchema;
export const Date = JONDateSchema;
export const Enum = JONEnumSchema;
export const NotInEnum = JONNotInEnumSchema;
export const ChosenType = JONChosenTypeSchema;
export const Object = JONObjectSchema;
export const Array = JONArraySchema;
export const File = JONFileSchema;
export const Any = JONAnySchema;
export const Clone = JONClone;
export const Ref = JONRef;

export const Validator = {
    validate: <
        T = any,
    > (
        schema: (
            JONNumberSchema |
            JONStringSchema |
            JONBooleanSchema |
            JONDateSchema |
            JONEnumSchema |
            JONObjectSchema |
            JONArraySchema |
            JONChosenTypeSchema |
            JONFileSchema |
            JONAnySchema
        ),
        value: T,
    ) => schema.validate(value),
    sanitize: <
        T = any,
    > (
        schema: (
            JONNumberSchema |
            JONStringSchema |
            JONBooleanSchema |
            JONDateSchema |
            JONEnumSchema |
            JONObjectSchema |
            JONArraySchema |
            JONChosenTypeSchema |
            JONFileSchema |
            JONAnySchema
        ),
        value: T,
    ) => schema.sanitize(value),
    isValid: <
        T = any,
    > (
        schema: (
            JONNumberSchema |
            JONStringSchema |
            JONBooleanSchema |
            JONDateSchema |
            JONEnumSchema |
            JONObjectSchema |
            JONArraySchema |
            JONChosenTypeSchema |
            JONFileSchema |
            JONAnySchema
        ),
        value: T,
    ) => schema.isValid(value),
};

const JON = {
    Validator,
    Number: JONNumberSchema,
    String: JONStringSchema,
    Boolean: JONBooleanSchema,
    Date: JONDateSchema,
    Enum: JONEnumSchema,
    NotInEnum: JONNotInEnumSchema,
    ChosenType: JONChosenTypeSchema,
    Array: JONArraySchema,
    Object: JONObjectSchema,
    File: JONFileSchema,
    Any: JONAnySchema,
    Clone: JONClone,
    Ref: JONRef,
};
export default JON;